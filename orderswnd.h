//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ORDERSWND_H
#define ORDERSWND_H

#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>

#include "setdriverwnd.h"
#include "makeorderwnd.h"
#include "user_input.h"

class ordersWnd : public QWidget
{
    Q_OBJECT
public:
    explicit ordersWnd(QWidget *parent = 0);
    QGridLayout *layout;
    QTableWidget *orders;
    QLabel *ordersLB;

    user_input *q;

    QPushButton *orderBtn;

    QList<shop> shopsList;
signals:

public slots:
    void orderBtnClicked();
    void setTable();
    void addDriverBtnClicked();
    void actionChanged(QString action);
};

#endif // ORDERSWND_H
