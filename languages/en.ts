<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>backupWnd</name>
    <message>
        <location filename="../backupwnd.cpp" line="7"/>
        <source>Backup database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backupwnd.cpp" line="8"/>
        <source>Restore database from backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backupwnd.cpp" line="24"/>
        <source>Choose directory to save backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backupwnd.cpp" line="46"/>
        <location filename="../backupwnd.cpp" line="75"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backupwnd.cpp" line="46"/>
        <source>You&apos;ve successfully created backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backupwnd.cpp" line="56"/>
        <source>Choose backup file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backupwnd.cpp" line="75"/>
        <source>You&apos;ve successfully restored database from backup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dailyStatWnd</name>
    <message>
        <location filename="../dailystatwnd.cpp" line="9"/>
        <source>Choose date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dailystatwnd.cpp" line="33"/>
        <source>Print Document</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>expenditureWidget</name>
    <message>
        <location filename="../expenditurewidget.cpp" line="9"/>
        <source>Products</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="10"/>
        <source>Business</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="11"/>
        <source>Private</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="12"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="17"/>
        <source>Ependiture text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="21"/>
        <source>Expenditure cost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="26"/>
        <source>Expenditure MwSt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="30"/>
        <location filename="../expenditurewidget.cpp" line="60"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="33"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="73"/>
        <source>Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditurewidget.cpp" line="125"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>expendituresWnd</name>
    <message>
        <location filename="../expenditureswnd.cpp" line="5"/>
        <location filename="../expenditureswnd.cpp" line="7"/>
        <source>Expenditures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditureswnd.cpp" line="13"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditureswnd.cpp" line="23"/>
        <source>Add new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../expenditureswnd.cpp" line="62"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mainWnd</name>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="68"/>
        <source>Reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="70"/>
        <source>Synchronize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="72"/>
        <source>Add expenditure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="75"/>
        <source>Download streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="78"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>makeOrderWnd</name>
    <message>
        <location filename="../makeorderwnd.cpp" line="16"/>
        <source>Product</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="18"/>
        <location filename="../makeorderwnd.cpp" line="122"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="30"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="31"/>
        <location filename="../makeorderwnd.cpp" line="888"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="32"/>
        <location filename="../makeorderwnd.cpp" line="884"/>
        <source>Quantity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="33"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="54"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="72"/>
        <source>Meals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="78"/>
        <location filename="../makeorderwnd.cpp" line="121"/>
        <source>Additions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="84"/>
        <location filename="../makeorderwnd.cpp" line="162"/>
        <location filename="../makeorderwnd.cpp" line="693"/>
        <location filename="../makeorderwnd.cpp" line="754"/>
        <source>Delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="89"/>
        <source>mwst 7 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="98"/>
        <source>MwSt 19 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="104"/>
        <source>Discount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="109"/>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="159"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="163"/>
        <location filename="../makeorderwnd.cpp" line="695"/>
        <source>Take away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="164"/>
        <location filename="../makeorderwnd.cpp" line="254"/>
        <location filename="../makeorderwnd.cpp" line="976"/>
        <location filename="../makeorderwnd.cpp" line="1091"/>
        <source>In Restaurant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="265"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="265"/>
        <source>Product in order table must be same as product in Products table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="771"/>
        <location filename="../makeorderwnd.cpp" line="856"/>
        <source>Print Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="830"/>
        <source>TOTAL: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="881"/>
        <source>Tel: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="892"/>
        <source>Summ/€</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="925"/>
        <source>MwSt. (7%): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="928"/>
        <source>MwSt. (19%): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../makeorderwnd.cpp" line="931"/>
        <source>Invoice amount: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>month_stat</name>
    <message>
        <location filename="../month_stat.cpp" line="27"/>
        <source>sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="28"/>
        <source>orders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="29"/>
        <source>Cash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="30"/>
        <source>Sellings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="31"/>
        <source>Delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="32"/>
        <source>Additionally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="34"/>
        <source>MwSt 7%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="35"/>
        <source>MwSt 19%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="56"/>
        <location filename="../month_stat.cpp" line="150"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="117"/>
        <source>Expenditures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../month_stat.cpp" line="121"/>
        <source>Profit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>order_dialog</name>
    <message>
        <location filename="../order_dialog.cpp" line="5"/>
        <source>Choose size and quantity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../order_dialog.cpp" line="13"/>
        <source>Size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../order_dialog.cpp" line="15"/>
        <source>Sort: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../order_dialog.cpp" line="38"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ordersWnd</name>
    <message>
        <location filename="../orderswnd.cpp" line="8"/>
        <source>Orders list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="11"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="18"/>
        <source>OrderID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="19"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="20"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="21"/>
        <source>Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="22"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="130"/>
        <source>Chose action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="131"/>
        <location filename="../orderswnd.cpp" line="264"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="132"/>
        <location filename="../orderswnd.cpp" line="192"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="182"/>
        <source>Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="182"/>
        <source>No driver to set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="193"/>
        <source>Delete confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="193"/>
        <source>Are you shure want to delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="210"/>
        <source>Edition confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="210"/>
        <source>Are you shure want to edit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="229"/>
        <source>Delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="231"/>
        <source>Take away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="233"/>
        <source>In Restaurant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="265"/>
        <source>Print confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../orderswnd.cpp" line="265"/>
        <source>Are you shure want to print?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>reportsWnd</name>
    <message>
        <location filename="../reportswnd.cpp" line="8"/>
        <source>Date start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="16"/>
        <source>Date end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="25"/>
        <source>Tax report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="28"/>
        <location filename="../reportswnd.cpp" line="144"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="38"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="39"/>
        <source>Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="40"/>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="41"/>
        <source>MwSt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="54"/>
        <source>Month report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="55"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="56"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="192"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../reportswnd.cpp" line="192"/>
        <source>Orders where saved to excel file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>setDriverWnd</name>
    <message>
        <location filename="../setdriverwnd.cpp" line="6"/>
        <source>Choose driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setdriverwnd.cpp" line="11"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>user_input</name>
    <message>
        <location filename="../user_input.cpp" line="6"/>
        <source>Choosing customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="36"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="38"/>
        <source>Telephone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="40"/>
        <source>User ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="43"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="45"/>
        <source>Street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="47"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="49"/>
        <source>note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="51"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="53"/>
        <source>Postal code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="80"/>
        <location filename="../user_input.cpp" line="115"/>
        <source>Delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="81"/>
        <location filename="../user_input.cpp" line="240"/>
        <source>Take away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="82"/>
        <location filename="../user_input.cpp" line="240"/>
        <source>In Restaurant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="119"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_input.cpp" line="119"/>
        <source>address must be longer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
