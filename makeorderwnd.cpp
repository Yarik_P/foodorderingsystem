#include "makeorderwnd.h"

makeOrderWnd::makeOrderWnd(QString order, QString orderTypeStr) : QWidget(0)
{
    if (order.length() > 0) {
        getOrder = true;
        orderStr = order;
    }
    this->orderTypeStr = orderTypeStr;
    german = QLocale(QLocale::German, QLocale::Germany);

    this->setWindowTitle("Make order");
    this->showMaximized();

    /* Meals */
    mealsGB = new QGroupBox(tr("Product"), this);
    mealsLay = new QGridLayout(mealsGB);
    productsSearchL = new QLabel(tr("Search"));
    productsSearchLE = new QLineEdit(this);

    searchNext = new QPushButton("->");
    connect(searchNext, SIGNAL(clicked()), this, SLOT(mealSearchNext()));
    searchPrevious = new QPushButton("<-");
    connect(searchPrevious, SIGNAL(clicked()), this, SLOT(mealSearchBack()));

    mealsTB = new QTableWidget(0, 2, this);
    mealsTB->verticalHeader()->setVisible(false);
    mealsTB->verticalHeader()->setDefaultSectionSize(23);

    QTableWidgetItem *head_id = new QTableWidgetItem(tr("Code"));
    QTableWidgetItem *head_name = new QTableWidgetItem(tr("Name"));
    QTableWidgetItem *head_quantity = new QTableWidgetItem(tr("Quantity"));
    QTableWidgetItem *head_price = new QTableWidgetItem(tr("Price"));


    mealsTB->setHorizontalHeaderItem(0, head_id);
    mealsTB->setHorizontalHeaderItem(1, head_name);
    mealsTB->horizontalHeader()->resizeSection(0, 60);
    mealsTB->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    mealsTB->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(mealsTB, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(OnTableDoubleclicked()));
    mealsLay->addWidget(productsSearchL, 1, 1);
    mealsLay->addWidget(productsSearchLE, 1, 2);
    mealsLay->addWidget(searchPrevious, 1, 3);
    mealsLay->addWidget(searchNext, 1, 4);
    mealsLay->addWidget(mealsTB, 2, 1, 1, 4);
    mealsGB->setLayout(mealsLay);
    /*/         /*/


    /* orders    */
    orderGB = new QGroupBox(tr("Order"), this);
    orderLay = new QGridLayout(orderGB);
    orderTB = new QTableWidget(0, 4, orderGB);
    orderTB->setSelectionBehavior(QAbstractItemView::SelectRows);

    orderTB->setHorizontalHeaderItem(0, head_id);
    orderTB->setHorizontalHeaderItem(1, head_name);
    orderTB->setHorizontalHeaderItem(2, head_quantity);
    orderTB->setHorizontalHeaderItem(3, head_price);

    orderTB->horizontalHeader()->resizeSection(0, 60);
    orderTB->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    orderTB->horizontalHeader()->resizeSection(2, 100);
    orderTB->horizontalHeader()->resizeSection(3, 100);
    orderTB->verticalHeader()->setDefaultSectionSize(23);

    orderLay->addWidget(orderTB, 1, 1, 1, 4);

    summL = new QLabel(tr("Meals"));
    summTE = new QLineEdit(orderGB);
    summTE->setReadOnly(true);
    orderLay->addWidget(summL, 2, 3);
    orderLay->addWidget(summTE, 2, 4);

    addittionsSummL = new QLabel(tr("Additions"));
    addittionsSummTE = new QLineEdit(orderGB);
    addittionsSummTE->setEnabled(false);
    orderLay->addWidget(addittionsSummL, 3, 3);
    orderLay->addWidget(addittionsSummTE, 3, 4);

    deliveryL = new QLabel(tr("Delivery"));
    deliveryTE = new QLineEdit(orderGB);
    orderLay->addWidget(deliveryL, 4, 3);
    orderLay->addWidget(deliveryTE, 4, 4);

    mwstL = new QLabel(tr("mwst 7 %"));
    mwstTE = new QLineEdit(orderGB);
    mwstTE->setReadOnly(true);
    mwstTE->setValidator(new QDoubleValidator(0, 100, 2, this));


    orderLay->addWidget(mwstL, 5, 3);
    orderLay->addWidget(mwstTE, 5, 4);

    mwst19L = new QLabel(tr("MwSt 19 %"));
    mwst19TE = new QLineEdit();
    mwst19TE->setReadOnly(true);
    orderLay->addWidget(mwst19L, 6, 3);
    orderLay->addWidget(mwst19TE, 6, 4);

    discountL = new QLabel(tr("Discount"));
    discountTE = new QLineEdit();
    orderLay->addWidget(discountL, 7, 3);
    orderLay->addWidget(discountTE, 7, 4);

    finalSumL = new QLabel(tr("Summary"));
    finalSumTE = new QLineEdit();
    finalSumTE->setReadOnly(true);
    orderLay->addWidget(finalSumL, 8, 3);
    orderLay->addWidget(finalSumTE, 8, 4);

    orderGB->setLayout(orderLay);

    /*/         /*/


    /* additions */
    additionsGB = new QGroupBox(tr("Additions"), this);
    addingsSearchL = new QLabel(tr("Search"));
    addingsSearchLE = new QLineEdit();
    additionsLay = new QGridLayout(additionsGB);
    additionsTB = new QTableWidget(0, 2, additionsGB);
    additionsTB->verticalHeader()->setVisible(false);
    additionsTB->setHorizontalHeaderItem(0, head_id);
    additionsTB->setHorizontalHeaderItem(1, head_name);

    additionsTB->horizontalHeader()->resizeSection(0, 60);
    additionsTB->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    additionsTB->verticalHeader()->setDefaultSectionSize(23);

    addSearchNext = new QPushButton("v", this);
    addSearchPrevious = new QPushButton("^", this);
    connect(addSearchNext, SIGNAL(clicked()), this, SLOT(additionsSearchNext()));
    connect(addSearchPrevious, SIGNAL(clicked()), this, SLOT(additionsSearchBack()));

    additionsTB->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(additionsTB, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(OnAddingsTableDoubleclicked()));

    additionsLay->addWidget(addingsSearchL, 1, 1);
    additionsLay->addWidget(addingsSearchLE, 1, 2);
    additionsLay->addWidget(addSearchNext, 1, 4);
    additionsLay->addWidget(addSearchPrevious, 1, 3);
    additionsLay->addWidget(additionsTB, 2, 1, 1, 4);
    additionsGB->setLayout(additionsLay);
    /*/         /*/


    /* global */
    layout = new QGridLayout(this);
    layout->addWidget(orderGB, 1, 1, 2, 2);
    layout->addWidget(mealsGB, 1, 3, 1, 2);
    layout->addWidget(additionsGB, 2, 3, 1, 2);


    printBtn = new QPushButton(tr("Print"), this);
    layout->addWidget(printBtn, 3, 4);
    orderType = new QComboBox(this);
    orderType->addItem(tr("Delivery"));
    orderType->addItem(tr("Take away"));
    orderType->addItem(tr("In Restaurant"));
    layout->addWidget(orderType, 3, 3);

    shopidCB = new QComboBox(this);

    layout->addWidget(shopidCB, 3, 2);

    this->setLayout(layout);


    connect(productsSearchLE, SIGNAL(textChanged(QString)), this, SLOT(meals_search(QString)));
    connect(addingsSearchLE, SIGNAL(textChanged(QString)), this, SLOT(meals_search(QString)));
    connect(deliveryTE, SIGNAL(textChanged(QString)), this, SLOT(deliveryCostChanged(QString)));
    connect(mealsTB, SIGNAL(clicked(QModelIndex)), this, SLOT(mealsTBClicked(QModelIndex)));
    connect(printBtn, SIGNAL(clicked()), this, SLOT(printBtnClicked()));
    connect(shopidCB, SIGNAL(currentTextChanged(QString)), this, SLOT(shopidChanged(QString)));

    int index = orderType->findText(orderTypeStr, Qt::MatchContains);

    orderType->setCurrentIndex(index);
    connect(orderType, SIGNAL(currentTextChanged(QString)), this, SLOT(orderTypeChanged(QString)));
    replying();
}

void makeOrderWnd::setProduct(QTableWidget *table, product prod)
{
    // adding product to list
    QTableWidgetItem *id = new QTableWidgetItem(prod.code);

    QTableWidgetItem *name = new QTableWidgetItem(prod.name);

    id->setFlags(id->flags() ^ Qt::ItemIsEditable);
    name->setFlags(name->flags() ^ Qt::ItemIsEditable);

    int row = table->rowCount();
    table->setRowCount(row + 1);
    table->setItem(row, 0, id);
    table->setItem(row, 1, name);
}

void makeOrderWnd::OnTableDoubleclicked()
{
    // slot that begins adding product to chart
    int indx = mealsTB->currentRow();
    product q = meals.at(indx);
    getProductSize(q);
}

void makeOrderWnd::OnAddingsTableDoubleclicked()
{
    // slot that adds adding to product in chart
    if (orderTB->currentRow() == -1) {
        QMessageBox::information(this, "Error", "choose product in order table to add addition");
        return;
    }
    QString orderTBtext = orderTB->item(orderTB->currentRow(), 0)->text();
    QString mealsTBtext = mealsTB->item(mealsTB->currentRow(), 0)->text();


    if (orderTBtext == mealsTBtext) {

        product prod = meals[mealsTB->currentRow()];
        if (prod.cat->category_toppings.length() == 0){
            return;
        }

        QString size = orderTB->item(orderTB->currentRow(), 1)->text().split("(").at(1).split(")").at(0);

        topping *top = prod.cat->category_toppings[additionsTB->currentRow()];

        int ind = orderTB->currentRow();
        orderTB->item(ind, 1)->setText(orderTB->item(ind, 1)->text() + " + " + top->name);

        int _quantity = orderTB->item(orderTB->currentRow(), 2)->text().toInt();

        float price_ = 0;

        if (size.length() == 0) {
            price_ = top->prices[prod.cat->sizes.keys().at(0)] * _quantity;
        } else {
            price_ = top->prices[prod.cat->sizes.key(size)] * _quantity;
        }

        QString priceText = german.toString(german.toFloat(orderTB->item(ind, 3)->text()) + price_, 'f', 2);
        orderTB->item(ind, 0)->setData(Qt::UserRole, QString::number(orderTB->item(ind, 0)->data(Qt::UserRole).toFloat() + price_, 'f', 2));

        orderTB->item(ind, 3)->setText(priceText);

        additionsSumm += price_;
        float mwstProm = 0;
        if (orderTypeStr == tr("In Restaurant") || !prod.cat->tax_7) {
            mwstProm = (price_ - price_ / 1.19) * _quantity;
            orderTB->item(ind, 3)->setData(Qt::UserRole, QString::number(orderTB->item(ind, 3)->data(Qt::UserRole).toFloat() +
                                                                         mwstProm, 'f', 2));
            mwst19 += mwstProm;
        } else {
            mwstProm = (price_ - price_ / 1.07) * _quantity;
            orderTB->item(ind, 2)->setData(Qt::UserRole, QString::number(orderTB->item(ind, 2)->data(Qt::UserRole).toFloat() +
                                                                         mwstProm, 'f', 2));
            mwst += mwstProm;
        }
        update_order();
    } else {
        QMessageBox::information(this, tr("Error"), tr("Product in order table must be same as product in Products table"));
    }
}

void makeOrderWnd::meals_search(QString text)
{
    // slot that holds search in meals
    if (text.length() == 0) {
        return;
    }
    int &pres = (sender() == this->productsSearchLE) ? mealsSearchPresizion : addingsSearchPresizion;
    QTableWidget *table;
    if (sender() == this->productsSearchLE) {
        table = this->mealsTB;
    } else {
        table = this->additionsTB;
    }
    QList<QTableWidgetItem *> LTempTable = table->findItems(text, Qt::MatchContains);
    if (text.length() == 0 || LTempTable.length() == 0) {
        for (int i = 0; i < table->rowCount(); i++) {
            for (int q = 0; q < 2; q++) {
                table->item(i, q)->setSelected(false);
            }
        }
    } else {
        for (int i = 0; i < table->rowCount(); i++) {
            table->item(i, 0)->setSelected(LTempTable.contains(table->item(i, 0)));
            table->item(i, 1)->setSelected(LTempTable.contains(table->item(i, 1)));
        }
        if (pres >= LTempTable.length()) {
            pres = 0;
        }
        if (pres < 0) {
            pres = LTempTable.length() - 1;
        }
        table->setCurrentItem(LTempTable[pres]);
    }

}

category *makeOrderWnd::get_category(int id) {
    // function that returns category of product
    for (int i = 0; i < categories.count(); i++) {
        if (categories.at(i)->id == id) {
            return categories.at(i);
        }
    }
    return categories.at(0);
}

void makeOrderWnd::replying() {
    // function that is parsing json file
    // adding products to table
    // sets order if was selected edit option in orders window
    QFile file("shops.json");
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        datas = stream.readAll().toUtf8();
    }

    QJsonDocument doc = QJsonDocument::fromJson(datas);
    QJsonObject object = doc.object();
    QJsonArray shops = object["shops"].toArray();


    foreach (QJsonValue shopJS, shops) {
        QJsonObject obj = shopJS.toObject();

        QJsonArray offersJS = obj["day_offers"].toArray();

        foreach (QJsonValue offerJSVal, offersJS) {
            QJsonObject offerJSObj = offerJSVal.toObject();

            int day = offerJSObj["day_of_week"].toInt();
            QDateTime begin = QDateTime::fromString(offerJSObj["time_from"].toString(), "yyyy-MM-ddThh:mm:ss");
            QDateTime end = QDateTime::fromString(offerJSObj["time_to"].toString(), "yyyy-MM-ddThh:mm:ss");
            QDateTime cur = QDateTime::currentDateTime();

            if (day == cur.date().dayOfWeek() && begin.time() < cur.time() && cur.time() < end.time()){
                activeOffer.category_id = offerJSObj["category_id"].toInt();
                activeOffer.price = offerJSObj["price"].toDouble();
                activeOffer.size_id = offerJSObj["size_id"].toInt();
            }
        }

        int id = obj["id"].toInt();
        QString name = obj["name"].toString();
        if (shopid < 0) {
            shopid = id;
        }
        if (shopidCB->findText(name) == -1) {
            shopidCB->addItem(name, id);
        }

        if (id == shopid && id != setid) {
            QString code = obj["code"].toString();
            QString street = obj["street"].toString();
            QString postal_code = obj["postal_code"].toString();
            QString city = obj["city"].toString();
            QString telephone = obj["telephone"].toString();
            QString fax = obj["fax"].toString();
            QString tax_number = obj["tax_number"].toString();
            QJsonArray drivers = obj["drivers"].toArray();

            shop *currentShop = new shop(id, name, code, street, postal_code, city, telephone, fax, tax_number);
            foreach (QJsonValue driverJS, drivers) {
                QJsonObject driver_obj = driverJS.toObject();
                driver *promDriver = new driver(driver_obj["id"].toString(), driver_obj["name"].toString(), driver_obj["telephone"].toString());
                currentShop->drivers.append(promDriver);
            }
            this->ThisShop = currentShop;

            QJsonArray categoriesJS = obj["categories"].toArray();


            foreach (QJsonValue categ, categoriesJS) {
                QJsonObject categ_obj = categ.toObject();
                QJsonArray sizes = categ_obj["category_sizes"].toArray();
                QJsonArray tops = categ_obj["category_toppings"].toArray();
                category *cat = new category(categ_obj["id"].toInt(), categ_obj["name"].toString(), categ_obj["tax"].toInt());

                foreach (QJsonValue sizeJS, sizes) {
                    QJsonObject size_obj = sizeJS.toObject();
                    QString name = size_obj["name"].toString();
                    if (name.length() > 0){
                        cat->sizes.insert(size_obj["id"].toInt(), name);
                    }
                }

                QJsonArray sorts = categ_obj["category_sorts"].toArray();
                foreach (QJsonValue sortJS, sorts) {
                    QJsonObject prod_obj = sortJS.toObject();
                    cat->sorts.append(prod_obj["name"].toString());
                }

                QJsonArray products = categ_obj["products"].toArray();
                foreach (QJsonValue prodJS, products) {
                    QJsonObject prod_obj = prodJS.toObject();
                    product prod = product(cat, prod_obj["id"].toInt(), prod_obj["code"].toString(), prod_obj["name"].toString());
                    QJsonArray prices = prod_obj["product_prices"].toArray();
                    foreach (QJsonValue price, prices) {
                        QJsonObject price_obj = price.toObject();
                        if (price_obj["product_id"].toInt() == prod.id) {
                            int sizeId = price_obj["size_id"].toInt();
                            QString size = prod.cat->sizes[sizeId];

                            if (cat->id == activeOffer.category_id && sizeId == activeOffer.size_id) {
                                if (size.length() > 0) {
                                    prod.prices.insert(size, activeOffer.price);
                                }

                            } else {
                                if (size.length() > 0) {
                                    prod.prices.insert(size, price_obj["price"].toDouble());
                                }
                            }
                        }
                    }
                    meals.append(prod);
                }
                foreach (QJsonValue topJS, tops) {
                    QJsonObject topping_obj = topJS.toObject();
                    topping *top = new topping(topping_obj["id"].toInt(), topping_obj["name"].toString(), topping_obj["category_id"].toInt());
                    QJsonArray top_sizes = topping_obj["topping_prices"].toArray();
                    for (int i = 0; i < top_sizes.count(); i++){
                        top->prices.insert(top_sizes.at(i).toObject()["size_id"].toInt(), top_sizes.at(i).toObject()["price"].toDouble());
                    }
                    cat->category_toppings.append(top);
                }
                categories.append(cat);
            }

            foreach (product prod, meals) {
                setProduct(mealsTB, prod);
            }
        }
    }
    // here begins setting order into chart
    if (getOrder) {
        QStringList order_values = orderStr.split(";");

        for (int i = 0; i < order_values.length(); i++) {
            QString q = order_values.at(i);
            QStringList prom = q.split(", ");
            if (prom.length() < 2) {
                break;
            }
            QStringList productStrList = prom.at(0).split("(");
            QString name = productStrList.at(0);

            QString size = productStrList.at(1).split(")").at(0);

            int quan = prom.at(1).toInt();

            add_to_order(name, size, quan);


            QStringList addings = productStrList.at(1).split(")").at(1).split(" + ");
            if (addings.length() > 0) {
                for (int j = 1; j < addings.length(); j++) {
                    QString addName = addings.at(j);
                    product *prod = new product(get_product(name));
                    topping *top;
                    for (int z = 0; z < prod->cat->category_toppings.length(); z++) {
                        if (prod->cat->category_toppings.at(z)->name == addName) {
                            top = prod->cat->category_toppings.at(z);
                        }
                    }
                    int ind = orderTB->rowCount() - 1;
                    orderTB->item(ind, 1)->setText(orderTB->item(ind, 1)->text() + " + " + top->name);

                    int _quantity = quan;

                    float price_ = top->prices[prod->cat->sizes.key(size)];

                    QString priceText = german.toString((orderTB->item(ind, 3)->text().toFloat()) + price_, 'f', 2);

                    if (priceText.contains(",")) {
                        priceText.append("0");
                    } else {
                        priceText.append(",00");
                    }
                    orderTB->item(ind, 3)->setText(priceText);

                    additionsSumm += price_;

                    if (prod->cat->tax_7) {
                        float mwstProm = (price_ - price_ / 1.07) * _quantity;
                        orderTB->item(ind, 2)->setData(Qt::UserRole, QString::number(orderTB->item(ind, 2)->data(Qt::UserRole).toFloat() +
                                                                                     mwstProm, 'f', 2));
                        mwst += mwstProm;
                    } else {
                        float mwstProm = (price_ - price_ / 1.19) * _quantity;;
                        orderTB->item(ind, 3)->setData(Qt::UserRole, QString::number(orderTB->item(ind, 3)->data(Qt::UserRole).toFloat() +
                                                                                     mwstProm, 'f', 2));
                        mwst19 += mwstProm;
                    }
                    update_order();
                }
            }
            productsSearchLE->clear();
        }
    }
    setid = shopid;
}

void makeOrderWnd::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Delete:
        remove_from_order();
        break;
    case Qt::Key_Enter:
        if(productsSearchLE->hasFocus()) {
            if (productsSearchLE->text().length() > 0){
                OnTableDoubleclicked();
                productsSearchLE->clear();
                mealsTBClicked(mealsTB->currentIndex());
                mealsTB->item(mealsTB->currentIndex().row(), 0)->setSelected(true);
                mealsTB->item(mealsTB->currentIndex().row(), 1)->setSelected(true);
            }
        }
        if(addingsSearchLE->hasFocus()) {
            addingsSearchLE->clear();
            OnAddingsTableDoubleclicked();
        }

        if (mealsTB->hasFocus()) {
            OnTableDoubleclicked();
        }
        if (additionsTB->hasFocus()) {
            OnAddingsTableDoubleclicked();
        }
        break;

    case Qt::Key_Return:
        if(productsSearchLE->hasFocus()) {
            if (productsSearchLE->text().length() > 0) {
                OnTableDoubleclicked();
                productsSearchLE->clear();
                mealsTBClicked(mealsTB->currentIndex());
                mealsTB->item(mealsTB->currentIndex().row(), 0)->setSelected(true);
                mealsTB->item(mealsTB->currentIndex().row(), 1)->setSelected(true);
            }
        }
        if(addingsSearchLE->hasFocus()) {
            addingsSearchLE->clear();
            OnAddingsTableDoubleclicked();
        }

        if (mealsTB->hasFocus()) {
            OnTableDoubleclicked();
        }
        if (additionsTB->hasFocus()) {
            OnAddingsTableDoubleclicked();
        }
        break;
    }
}

void makeOrderWnd::deliveryCostChanged(QString text)
{
    deliverySum = text.toFloat();
    update_order();
}

void makeOrderWnd::getProductSize(product _product)
{
    order_dialog *q = new order_dialog(_product, this);
    connect(q, SIGNAL(send_product(QString&,QString,int,QString)), this, SLOT(add_to_order(QString&,QString,int,QString)));
    q->exec();
}

void makeOrderWnd::mealsTBClicked(QModelIndex ind)
{
    if (additionsTB->rowCount() > 1) {
        for (int i = 0; i < additionsTB->rowCount(); i++) {
            additionsTB->removeRow(i);
        }
        additionsTB->setRowCount(0);
    }
    product prod = get_product(mealsTB->item(ind.row(), 1)->text());

    for (int i = 0; i < prod.cat->category_toppings.count(); i++) {
        additionsTB->setRowCount(i + 1);
        QTableWidgetItem *id = new QTableWidgetItem(QString::number(prod.cat->category_toppings.at(i)->id));
        id->setFlags(id->flags() ^ Qt::ItemIsEditable);
        additionsTB->setItem(i, 0, id);
        QTableWidgetItem *name = new QTableWidgetItem(prod.cat->category_toppings.at(i)->name);
        name->setFlags(name->flags() ^ Qt::ItemIsEditable);
        additionsTB->setItem(i, 1, name);
    }
}

void makeOrderWnd::printBtnClicked()
{
    // slot that inserts/updates order into database and calls print function
    if (orderTB->rowCount() == 0) {
        return;
    }

    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
        qDebug() << sdb.lastError().text();
    }

    QSqlQuery a_query;

    if (!getOrder) {
        a_query.exec("select order_uid, max(`_rowid_`) from orders where 1;");
        a_query.next();

        QStringList lastUid = a_query.value(0).toString().split("-");
        if (lastUid.at(0).toInt() < QDate::currentDate().year()) {
            order_uid = QString::number(QDate::currentDate().year()) + "-1";
        } else {
            order_uid = lastUid.at(0) + "-" + QString::number(lastUid.at(1).toInt() + 1);
        }
    }

    QList<int> rowsToDelete;
    for (int row = 0; row < orderTB->rowCount(); row++) {
        for (int row2 = 0; row2 < orderTB->rowCount(); row2++) {
            if (row != row2 && !rowsToDelete.contains(row) && !rowsToDelete.contains(row2)  && orderTB->item(row, 1)->text() ==
                    orderTB->item(row2, 1)->text()) {
                orderTB->item(row, 2)->setText(QString::number(orderTB->item(row, 2)->text().toInt() + orderTB->item(row2, 2)->text().toInt()));
                rowsToDelete.append(row2);
                for (int i = 0; i < 4; i++) {
                    orderTB->item(row, i)->setData(Qt::UserRole, orderTB->item(row, i)->data(Qt::UserRole).toFloat() +
                                                   orderTB->item(row2, i)->data(Qt::UserRole).toFloat());
                }
            }
        }
    }

    for (int i = rowsToDelete.length() - 1 ; i >= 0; i--) {
        orderTB->removeRow(rowsToDelete.at(i));
    }

    a_query.exec("select `order_count` from `Users` WHERE `user_uuid`='" + userId + "';");
    a_query.next();

    int orderCounter = a_query.value(0).toInt() + 1;
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("Bonuses");
    QStringList keys = settings.childKeys();
    QMap<int, QString> bonuses;
    foreach (QString key, keys) {
        bonuses[key.toInt()] = settings.value(key).toString();
    }

    for (int i = 0; i < bonuses.keys().length(); i++) {
        if (summ > bonuses.keys().at(i)) {

            int lastRow = orderTB->rowCount() + 1;
            orderTB->setRowCount(lastRow);
            QTableWidgetItem *promt = new QTableWidgetItem(bonuses[bonuses.keys().at(i)]);
            promt->setFlags(promt->flags() ^ Qt::ItemIsEditable);
            orderTB->setItem(lastRow - 1, 1, promt);

            QTableWidgetItem *id = new QTableWidgetItem(QString::number(0));
            id->setFlags(promt->flags());
            orderTB->setItem(lastRow - 1, 0, id);

            QTableWidgetItem *quantity = new QTableWidgetItem(QString::number(1));
            quantity->setFlags(promt->flags());
            orderTB->setItem(lastRow - 1, 2, quantity);

            QTableWidgetItem *price = new QTableWidgetItem(QString::number(0));
            price->setFlags(promt->flags());
            orderTB->setItem(lastRow - 1, 3, price);
        }
    }
    settings.endGroup();

    orderJson = "";
    // order json struct
    // product_line, quantity, mwst7, mwst19, sum;
    for (int i = 0; i < orderTB->rowCount(); i++) {
        QString prom = orderTB->item(i, 1)->text();
        int quantity = orderTB->item(i, 2)->text().toInt();
        float mwstprom = orderTB->item(i, 2)->data(Qt::UserRole).toFloat();
        float mwst19prom = orderTB->item(i, 3)->data(Qt::UserRole).toFloat();
        orderJson += prom + ", " + QString::number(quantity) + ", " + QString::number(mwstprom, 'f', 2) + ", " +
                QString::number(mwst19prom, 'f', 2) + ", " + QString::number(german.toFloat(orderTB->item(i, 3)->text()), 'f', 2) + ";";
    }


    int orderTypeInt = 0;

    if(orderTypeStr == tr("Delivery")) {
        orderTypeInt = 1;
    } else if (orderTypeStr == tr("Take away")) {
        orderTypeInt = 2;
    } else {
        orderTypeInt = 3;
    }
    if (!getOrder) {
        a_query.prepare("INSERT INTO `orders` (`user_uuid`, `summ`, `date`, `order`, `order_uid`, `customer_name`, `customer_address`, "
                        "`order_type`, `mwst7`, `mwst19`, `shop_id`) VALUES (:user_uuid, :summ, :date, :order, :uid, :name, :address, "
                        ":orderType, :mwst7, :mwst19, :shopid);");
        a_query.bindValue(":user_uuid", userId);
        a_query.bindValue(":summ", QString::number(finalSum, 'f', 2));
        a_query.bindValue(":date", QDate::currentDate());
        a_query.bindValue(":order", orderJson);
        a_query.bindValue(":name", username);
        a_query.bindValue(":address", address);
        a_query.bindValue(":orderType", orderTypeInt);
        a_query.bindValue(":uid", order_uid);
        a_query.bindValue(":mwst7", QString::number(mwst, 'f', 2));
        a_query.bindValue(":mwst19", QString::number(mwst19, 'f', 2));
        a_query.bindValue(":shopid", shopid);
        if(!a_query.exec()) {
            qDebug() << "Aerror:  " << a_query.lastError();
        }

        a_query.prepare("UPDATE `Users` SET `order_count`=`order_count`+1 WHERE `user_uuid`='" + userId + "';");
        if(!a_query.exec()) {
            qDebug() << "Berror:  " << a_query.lastError();
        }

        a_query.prepare("UPDATE `Users` SET `last_ordered_at`='" +
                        QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") + "' WHERE `user_uuid`='" + userId + "';");
        if(!a_query.exec()) {
            qDebug() << "Cerror:  " << a_query.lastError();
        }

        a_query.prepare("UPDATE `Users` set `bonus_count` = CASE WHEN `order_count`%10=0 and `order_count`!= 0 THEN "
                        "(`bonus_count`+1) ELSE `bonus_count` END WHERE `user_uuid`='" + userId + "';");
        if(!a_query.exec()) {
            qDebug() << "Derror:  " << a_query.lastError();
        }
    } else {
        a_query.prepare("UPDATE `orders` set `order_type`=" + QString::number(orderTypeInt) + ", `summ`='" + QString::number(finalSum, 'f', 2) +
                        "', `order`='" + orderJson + "' WHERE `order_uid` LIKE '" + order_uid + "';");
        if(!a_query.exec()) {
            qDebug() << "Aerror:  " << a_query.lastError();
        }
    }

    a_query.exec("select `order_count` from 'Users' WHERE `user_uuid`='" + userId + "';");
    sdb.close();

    QMap<QString, int> ordersMap;

    for (int i = 0; i < orderTB->rowCount(); i++) {
        QString order = orderTB->item(i, 1)->text() + " / "  + QString::number(german.toFloat(orderTB->item(i, 3)->text()), 'f', 2);

        int quantity = orderTB->item(i, 2)->text().toInt();
        if (ordersMap.keys().contains(order)) {
            ordersMap[order] += quantity;
        } else {
            ordersMap.insert(order, quantity);
        }
    }

    user Usr = user(this->username, this->address, this->post_code, this->city, this->user_phone);
    bool showInfo = orderTypeStr == tr("Delivery") ? true : false;

    if (settings.value("paper_type") == "monoprint") {
        print_order2(ordersMap, *this->ThisShop, this->summTE->text(), orderCounter);
    } else {
        print_order(ordersMap, *this->ThisShop, Usr, this->mwstTE->text(), this->mwst19TE->text(), this->summTE->text(), showInfo);
    }
    this->close();
}

void makeOrderWnd::print_order2(QMap<QString, int> orders, shop ThisShop, QString sumStr, int orderCounter) {
    QPrinter printer;

    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle(tr("Print Document"));

    if (dialog->exec() != QDialog::Accepted) {
        return;
    }
    QLocale german(QLocale::German, QLocale::Germany);
    QPainter *painter = new QPainter();
    QList<QPixmap> document;

    QFont font;
    font.setPixelSize(16);

    QFontMetrics metrics(font);


    QString text = ThisShop.name + "\n" + ThisShop.street + "\n" + ThisShop.postal_code + " " + ThisShop.city + "\nTel: " +
            ThisShop.telephone + "\nFax: " + ThisShop.fax;
    QRect boundRect = metrics.boundingRect(QRect(0, 0, 350, 350), Qt::TextWordWrap, text);
    QRectF *rect = new QRectF(0, 10, 360, boundRect.height());
    QPixmap qpix(360, boundRect.height() + 15);
    qpix.fill();
    painter->begin(&qpix);
    painter->setFont(font);

    painter->drawText(*rect, Qt::AlignHCenter, text);

    painter->end();
    document.append(qpix);

    for (int i = 0; i < orders.count(); i++) {

        QString key = orders.keys().at(i);
        QStringList order = key.split("/");

        int quantity = orders[key];

        QString text = order.at(0);

        float summFL = order.at(1).toFloat();
        if (summFL == 0) {
            summFL = german.toFloat(order.at(1));
        }

        QString summSTR = german.toString(summFL, 'f', 2) + " EUR";
        float price = summFL / quantity;

        boundRect = metrics.boundingRect(QRect(0, 0, 70, 300), Qt::TextWordWrap, summSTR);
        QRect boundRect1 = metrics.boundingRect(QRect(0, 0, 350, 300), Qt::TextWordWrap, text);

        int curHeight = boundRect.height();
        if (boundRect1.height() > curHeight){
            curHeight = boundRect1.height();
        }
        if (curHeight < 37){
            curHeight = 37;
        }

        QPixmap qpix(360, curHeight);
        qpix.fill();
        painter->begin(&qpix);
        painter->setFont(font);

        QRectF *smRect = new QRectF(10, 0, 240, curHeight);
        painter->drawText(*smRect, text);
        delete smRect;
        smRect = new QRectF(255, 0, 70, curHeight);

        painter->drawText(*smRect, QString::number(quantity) + " x " + german.toString(price, 'f', 2) + " " + summSTR);
        painter->end();
        document.append(qpix);
        delete smRect;
    }

    qpix = QPixmap(360, 65);
    qpix.fill();
    painter->begin(&qpix);
    painter->setFont(font);
    painter->drawText(4, 0, 360, 25, Qt::AlignCenter, QDateTime::currentDateTime().toString("hh:mm:ss dd-MM-yyyy"));
    painter->drawText(4, 28, 360, 25, Qt::AlignCenter, tr("TOTAL: ") + german.toString(sumStr.toFloat(), 'f', 2) + " Eur");
    painter->drawText(4, 42, 360, 29, Qt::AlignCenter, tr("This was your ") + QString::number(orderCounter) + " order.");
    painter->end();
    document.append(qpix);


    int heightCount = 0;
    for (int i = 0; i < document.count(); i++) {
        heightCount += static_cast<int>(document.at(i).heightMM());
    }

    printer.setPaperSize(QSizeF(80, heightCount/2), QPrinter::Millimeter);

    painter->begin(&printer);
    painter->setFont(font);
    heightCount = -15;
    for (int i = 0; i < document.count(); i++) {
        painter->drawPixmap(0, heightCount, static_cast<int>(document.at(i).width() / 2), static_cast<int>(document.at(i).height()/2),
                            document.at(i));
        heightCount += static_cast<int>(document.at(i).height()/2);
    }
    painter->end();
    delete painter;
    delete rect;
}


void makeOrderWnd::print_order(QMap<QString, int> orders, shop ThisShop, user Usr, QString mwst7str, QString mwst19str, QString sumStr,
                               bool showInfo){
    // function of print on standard A4 format
    QPrinter printer;
    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle(tr("Print Document"));

    if (dialog->exec() != QDialog::Accepted) {
        return;
    }

    QPainter painter;
    painter.begin(&printer);

    // Draw Header
    QString text = ThisShop.name + " | " + ThisShop.street + " | " + ThisShop.postal_code + " " + ThisShop.city;
    int offset = 210;
    painter.drawText(100, 50, 350, 100, Qt::AlignLeft|Qt::AlignTop, text);
    painter.drawText(100, 70, 350, 100, Qt::AlignLeft|Qt::AlignTop, Usr.username);
    if (showInfo) {
        painter.drawText(100, 90, 600, 100, Qt::AlignLeft|Qt::AlignTop, Usr.address);
        painter.drawText(100, 110, 350, 100, Qt::AlignLeft|Qt::AlignTop, Usr.post_code + " " + Usr.city);
    } else {
        offset -= 15;
    }

    painter.drawText(100, 110, 550, 100, Qt::AlignRight|Qt::AlignTop, QDateTime::currentDateTime().toString("hh:mm dd.MM.yyyy"));
    if (!showInfo) {
        offset -= 15;
    } else {
        painter.drawText(100, 130, 350, 100, Qt::AlignLeft|Qt::AlignTop, tr("Tel: ") + Usr.user_phone);
    }

    text = tr("Quantity");
    painter.drawText(75, offset - 50, 75, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
    painter.drawRect(75, offset - 50, 75, 50);

    text = tr("Name");
    painter.drawText(150, offset - 50, 465, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
    painter.drawRect(150, offset - 50, 470, 50);

    text = tr("Summ/€");

    painter.drawText(620, offset - 50, 100, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
    painter.drawRect(620, offset - 50, 100, 50);

    int i;

    QLocale *grm = new QLocale(QLocale::German, QLocale::Germany);
    for (i = 0; i < orders.count(); i++) {

        QStringList first = orders.keys().at(i).split(" / ");
        QString spl = first.at(1);

        text = grm->toString(orders[orders.keys().at(i)]);

        painter.drawText(75, 50 * i + offset, 75, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
        painter.drawRect(75, 50 * i + offset, 75, 50);

        text = first.at(0);
        QRectF *rect = new QRectF(150, 50 * i + offset, 470, 50);
        painter.drawRect(*rect);
        if (text.length() > 67) {
            QRectF *smRect = new QRectF(155, 50 * i + offset + 3, 460, 50);
            painter.drawText(*smRect, text);
        } else {
            painter.drawText(155, 50 * i + offset + 15, 460, 50, Qt::AlignLeft, text);
        }

        text = grm->toString(spl.toFloat() * orders[orders.keys().at(i)], 'f', 2) + " EUR";
        painter.drawText(620, 50 * i + offset, 100, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
        painter.drawRect(620, 50 * i + offset, 100, 50);
    }
    painter.drawRect(75, 50 * i + offset, 645, 50);
    painter.drawText(75, 50 * i + offset, 640, 50, Qt::AlignCenter|Qt::AlignRight, tr("MwSt. (7%): ") + mwst7str);
    i++;
    painter.drawRect(75, 50 * i + offset, 645, 50);
    painter.drawText(75, 50 * i + offset, 640, 50, Qt::AlignCenter|Qt::AlignRight, tr("MwSt. (19%): ") + mwst19str);
    i++;
    painter.drawRect(75, 50 * i + offset, 645, 50);
    painter.drawText(75, 50 * i + offset, 640, 50, Qt::AlignCenter|Qt::AlignRight, tr("Invoice amount: ") + sumStr);

    painter.end();
}


void makeOrderWnd::mealSearchNext()
{
    // this, and next 3 functions is shitty code
    // please forgive me about that
    // my manager made me to write this
    mealsSearchPresizion += 1;
    productsSearchLE->textChanged(productsSearchLE->text());
    mealsTB->setFocus();
}

void makeOrderWnd::mealSearchBack()
{
    mealsSearchPresizion -= 1;
    productsSearchLE->textChanged(productsSearchLE->text());
    mealsTB->setFocus();
}

void makeOrderWnd::additionsSearchNext()
{
    addingsSearchPresizion += 1;
    addingsSearchLE->textChanged(addingsSearchLE->text());
    additionsTB->setFocus();
}

void makeOrderWnd::additionsSearchBack()
{
    addingsSearchPresizion -= 1;
    addingsSearchLE->textChanged(addingsSearchLE->text());
    additionsTB->setFocus();
}


void makeOrderWnd::orderTypeChanged(QString text)
{
    // slot that holds changing of order type
    // its recalculationg all the VAT numbers, stored in order table items in Qt::UserRole data
    mwst19 = 0;
    mwst = 0;
    orderTypeStr = text;
    // 7% - quantity 19 % - price
    if (orderTypeStr == tr("In Restaurant")) {
        for (int i = 0; i < orderTB->rowCount(); i++) {
            orderTB->item(i, 2)->setData(Qt::UserRole, 0);
            float price = german.toFloat(orderTB->item(i, 3)->text());
            float promMwst19 = price - price / 1.19;
            mwst19 += promMwst19;
            orderTB->item(i, 3)->setData(Qt::UserRole, promMwst19);
        }
    } else {
        for (int i = 0; i < orderTB->rowCount(); i++){
            float promMwst = 0;
            float promMwst19 = 0;
            QStringList orderMeal = orderTB->item(i, 1)->text().split("(");
            QStringList orderAddings = orderMeal.at(1).split(")").at(1).split(" + ");

            product prod = get_product(orderMeal.at(0));
            float prc = prod.prices[orderMeal.at(1).split(")").at(0)];
            if (prod.cat->tax_7){
                promMwst = (prc - prc / 1.07) * orderTB->item(i, 2)->text().toInt();
            } else {
                promMwst19 = (prc - prc / 1.19) * orderTB->item(i, 2)->text().toInt();
            }
            for (int i = 1; i < orderAddings.length(); i++) {
                for (int j = 0; j < prod.cat->category_toppings.length(); j++) {
                    if (orderAddings.at(i) == prod.cat->category_toppings.at(j)->name) {
                        float prc = prod.cat->category_toppings.at(j)->prices[prod.cat->sizes.key(orderMeal.at(1).split(")").at(0))];
                        if (prod.cat->tax_7) {
                            promMwst += (prc - prc / 1.07) * orderTB->item(i, 2)->text().toInt();
                        } else {
                            promMwst19 += (prc - prc / 1.19) * orderTB->item(i, 2)->text().toInt();
                        }
                    }
                }
            }
            mwst += promMwst;
            mwst19 += promMwst19;
            orderTB->item(i, 2)->setData(Qt::UserRole, promMwst);
            orderTB->item(i, 3)->setData(Qt::UserRole, promMwst19);
        }
    }
    update_order();
}

void makeOrderWnd::shopidChanged(QString q) {
    // slot that holds changing shop id
    // clears meals table and calls replying function
    for (int i = 0; i < shopidCB->count(); i++) {
        if (shopidCB->itemText(i) == q) {
            shopid = shopidCB->itemData(i).toInt();
        }
    }

    if (additionsTB->rowCount() > 1) {
        for (int i = 0; i < additionsTB->rowCount(); i++) {
            additionsTB->removeRow(i);
        }
        additionsTB->setRowCount(0);
    }
    if (mealsTB->rowCount() > 1) {
        for (int i = 0; i < mealsTB->rowCount(); i++) {
            mealsTB->removeRow(i);
        }
        mealsTB->setRowCount(0);
    }
    meals.clear();

    replying();
}

void makeOrderWnd::add_to_order(QString& _prod, QString size, int _quantity, QString sort)
{
    // adding product to cart
    int lastRow = orderTB->rowCount() + 1;
    orderTB->setRowCount(lastRow);

    product *prod = new product(get_product(_prod));

    if (sort.length() != 0) {
        _prod += " /" + sort;
    }
    QTableWidgetItem *promt;
    if (size.length() != 0) {
        promt = new QTableWidgetItem(_prod + "(" + size + ")");
    } else {
        promt = new QTableWidgetItem(_prod);
    }
    orderTB->setItem(lastRow - 1, 1, promt);
    promt->setFlags(promt->flags() ^ Qt::ItemIsEditable);

    QTableWidgetItem *id = new QTableWidgetItem(prod->code);
    orderTB->setItem(lastRow - 1, 0, id);
    id->setFlags(promt->flags());

    QTableWidgetItem *quantity = new QTableWidgetItem(QString::number(_quantity));
    quantity->setFlags(promt->flags());


    orderTB->setItem(lastRow - 1, 2, quantity);
    float price_ = 0;
    if (size.length() == 0) {
        price_ = prod->prices.values().at(0);
    } else {
        price_ = prod->prices[size];
    }


    QTableWidgetItem *price = new QTableWidgetItem(german.toString(price_ * _quantity, 'f', 2));
    price->setFlags(promt->flags());
    orderTB->setItem(lastRow - 1, 3, price);
    summ += price_ * _quantity;

    // store hidden VAT information
    // 7 % in quantit->data(Qt::UserRole).toFloat()
    // 19 % in price->data(Qt::UserRole).toFloat()
    float mwstProm;
    if (orderTypeStr == tr("In Restaurant") || !prod->cat->tax_7) {
        mwstProm = (price_ - price_ / 1.19) * _quantity;
        price->setData(Qt::UserRole, QString::number(price->data(Qt::UserRole).toFloat() + mwstProm, 'f', 2));
        mwst19 += mwstProm;
    } else {
        mwstProm = (price_ - price_ / 1.07) * _quantity;
        quantity->setData(Qt::UserRole, QString::number(quantity->data(Qt::UserRole).toFloat() + mwstProm, 'f', 2));
        mwst += mwstProm;
    }

    orderTB->selectRow(orderTB->rowCount() - 1);
    update_order();
}

void makeOrderWnd::remove_from_order() {
    // removes position from order with all of it's addings and recalculates VAT summ
    int row = orderTB->currentRow();
    summ -= german.toFloat(orderTB->item(row, 3)->text());

    additionsSumm -= orderTB->item(row, 0)->data(Qt::UserRole).toFloat();
    this->mwst -= orderTB->item(row, 2)->data(Qt::UserRole).toFloat();
    this->mwst19 -= orderTB->item(row, 3)->data(Qt::UserRole).toFloat();

    orderTB->removeRow(row);
    update_order();
}

product makeOrderWnd::get_product(QString name) {
    // returns product by name
    for(int i = 0; i < meals.count(); i++) {
        if (meals[i].name == name) {
            return meals.at(i);
        }
    }
    return meals.at(0);
}

void makeOrderWnd::update_order() {
    // udpates VAT line edits
    if (summ < 0.01) {
        summ = 0;
    }
    if (mwst < 0.001 || summ < mwst) {
        mwst = 0;
    }
    if (mwst19 < 0.001 || summ < mwst19) {
        mwst19 = 0;
    }
    if (additionsSumm < 0.01) {
        additionsSumm = 0;
    }
    this->summTE->setText(german.toString(summ, 'f', 2));
    this->addittionsSummTE->setText(german.toString(additionsSumm, 'f', 2));
    this->mwstTE->setText(german.toString(mwst, 'f', 2));
    this->mwst19TE->setText(german.toString(mwst19, 'f', 2));
    finalSum = summ + additionsSumm + deliverySum;
    finalSum = QString::number(finalSum, 'f', 2).toFloat();
    if (finalSum < 0.01) {
        finalSum = 0;
    }
    this->finalSumTE->setText(german.toString(finalSum, 'f', 2));
}

makeOrderWnd::~makeOrderWnd() {
}
