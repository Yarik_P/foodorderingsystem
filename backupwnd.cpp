#include "backupwnd.h"

backupWnd::backupWnd() : QWidget(0)
{
    layout = new QGridLayout(this);

    storeBtn = new QPushButton(tr("Backup database"), this);
    reStoreBtn = new QPushButton(tr("Restore database from backup"), this);

    layout->addWidget(storeBtn, 1, 1);
    layout->addWidget(reStoreBtn, 2, 1);


    connect(storeBtn, SIGNAL(clicked()), this, SLOT(storeDB()));
    connect(reStoreBtn, SIGNAL(clicked()), this, SLOT(reStoreDB()));
}

void backupWnd::storeDB()
{
    /* Function that organizes backup of database */
    QFileDialog *dial = new QFileDialog();
    dial->setAcceptMode(QFileDialog::AcceptSave);
    dial->setWindowTitle(tr("Choose directory to save backup"));
    dial->selectFile("azzipdb_backup.db");
    QString path;

    if (dial->exec()) {
        QStringList fileNames = dial->selectedFiles();
        path = fileNames.first();
    }
    if (path.length() < 1) {
        dial->setResult(QFileDialog::Reject);
    }
    if (dial->result() == QFileDialog::Reject) {
        return;
    }

    if (QFile(path).exists()){
        QFile(path).remove();
    }

    copyDB("azzip.db", path);

    QFile::rename(path.split("/").last(), path);
    QMessageBox::information(this, tr("Success"), tr("You've successfully created backup"));
    this->close();
}

void backupWnd::reStoreDB()
{
    /* Function that organizes restoring database from backup */
    QFileDialog *dial = new QFileDialog();
    dial->setFilter(QDir::Drives);
    dial->setAcceptMode(QFileDialog::AcceptOpen);
    dial->setWindowTitle(tr("Choose backup file"));
    dial->selectFile("azzipdb_backup.db");
    QString path;

    if (dial->exec()) {
        QStringList fileNames = dial->selectedFiles();
        path = fileNames.first();

    }
    if (path.length() < 1) {
        dial->setResult(QFileDialog::Reject);
    }
    if (dial->result() == QFileDialog::Reject) {
        return;
    }

    QFile::copy(path, path.split("/").last());
    copyDB(path, "azzip.db");
    QFile::remove(path);
    QMessageBox::information(this, tr("Success"), tr("You've successfully restored database from backup"));
    this->close();
}

void backupWnd::copyDB(QString srcDB, QString destDB) {
    /* Function that copies one database into another and creates tables if needed */
    QSqlDatabase sdb  =QSqlDatabase::addDatabase("QSQLITE", "db1");
    sdb.setDatabaseName(srcDB);
    if (!sdb.open()) {
          qDebug() << sdb.lastError().text() << "12";
    }

    QSqlDatabase secdb =QSqlDatabase::addDatabase("QSQLITE", "db2");
    secdb.setDatabaseName(destDB);
    if (!secdb.open()) {
          qDebug() << secdb.lastError().text() << "123";
    }

    QSqlQuery query(sdb);
    QSqlQuery insertQuery(secdb);

    QString str = "CREATE TABLE IF NOT EXISTS 'Users' ( `user_uuid` TEXT NOT NULL UNIQUE, `mobile` NUMERIC NOT NULL, `postal_code` TEXT "
                  "DEFAULT NULL, `Name` TEXT, `Street` TEXT, `Home` TEXT, `note` TEXT, `comment` TEXT, `order_count` INTEGER DEFAULT 0, "
                  "`bonus_count` INTEGER DEFAULT 0, `company` TEXT DEFAULT NULL, `department` TEXT DEFAULT NULL, `last_ordered_at` TEXT, "
                  "`created` TEXT, `modified` TEXT, `city` TEXT );";

    if (!insertQuery.exec(str)) {
        qDebug() << insertQuery.lastError().text() << "table not created";
    }

    query.exec("select * from Users where 1");
    QString text;
    while (query.next()) {
        QString row;
        for (int i = 0; i < 16; i++) {
            text = query.value(i).toString();
            if (text.length() > 0){
                row += '"' + text+ '"' + ",";
            } else {
                row += "NULL,";
            }
        }
        row.chop(1);
        str = "INSERT INTO `Users`(`user_uuid`,`mobile`,`postal_code`, `Name`,`Street`,`Home`,`note`,`comment`,`order_count`,`bonus_count`,"
              "`company`,`department`,`last_ordered_at`,`created`,`modified`,`city`) VALUES (" + row + ");";
        if (!insertQuery.exec(str)) {
            qDebug() << insertQuery.lastError().text();
            qDebug() << str;
        }
    }

    str = "CREATE TABLE IF NOT EXISTS 'orders' ( `order_uid` TEXT UNIQUE, `user_uuid` TEXT, `summ` REAL, `date` TEXT, `order` TEXT, "
          "`order_deleted` INTEGER DEFAULT 0, `driver_id` TEXT, `customer_name` TEXT, `customer_address` TEXT, `order_type` TEXT, `mwst7` TEXT, "
          "`mwst19` TEXT, `shop_id` INTEGER );";
    if (!insertQuery.exec(str)) {
        qDebug() << insertQuery.lastError().text();
        qDebug() << str;
    }

    query.exec("select * from orders where 1;");

    while (query.next()) {
        QString row;
        for (int i = 0; i < 13; i++){
            text = query.value(i).toString();
            if (text.length() > 0){
                row += '"' + text + '"' + ",";
            } else {
                row += "NULL,";
            }
        }
        row.chop(1);

        if(!insertQuery.exec("INSERT INTO `orders`(`order_uid`,`user_uuid`,`summ`,`date`,`order`,`order_deleted`,`driver_id`,`customer_name`,"
                             "`customer_address`,`order_type`,`mwst7`,`mwst19`,`shop_id`) VALUES (" + row + ");")){
            qDebug() << insertQuery.lastError();
        }
    }

    str = "CREATE TABLE IF NOT EXISTS `days` ( `date` TEXT UNIQUE, `cash_begin` REAL, `cash_end` REAL )";
    if (!insertQuery.exec(str)) {
        qDebug() << insertQuery.lastError().text();
    } else {
        /*str = "INSERT INTO `days`(`date`,`cash_begin`) VALUES ('2017-04-21',0.0);";
        if (!insertQuery.exec(str)){
            qDebug() << query.lastError().text();
        }*/
    }
    query.exec("select * from days where 1;");

    while (query.next()) {
        QString row;
        qDebug() << query.value(0).toString();
        for (int i = 0; i < 3; i++) {
            text = query.value(i).toString();
            if (text.length() > 0) {
                row += '"' + text + '"' + ",";
            } else {
                row += "NULL,";
            }
        }
        row.chop(1);

        if(!insertQuery.exec("INSERT INTO `days`(`date`,`cash_begin`,`cash_end`) VALUES (" + row + ");")) {
            qDebug() << insertQuery.lastError();
        }
    }

    str = "CREATE TABLE IF NOT EXISTS `expenditures2` ( `date` TEXT, `text` TEXT, `summ` REAL, `tax` REAL, `type` INTEGER, `exp_uid` TEXT UNIQUE)";
    if (!insertQuery.exec(str)) {
        qDebug() << insertQuery.lastError().text();
    }

    query.exec("select * from expenditures2 where 1;");

    while (query.next()) {
        QString row;
        for (int i = 0; i < 6; i++) {
            text = query.value(i).toString();
            if (text.length() > 0) {
                row += '"' + text + '"' + ",";
            } else {
                row += "NULL,";
            }
        }
        row.chop(1);

        if(!insertQuery.exec("INSERT INTO `expenditures2`(`date`,`text`,`summ`,`tax`,`type`,`exp_uid`) VALUES (" + row + ");")) {
            qDebug() << insertQuery.lastError();
        }
    }
    sdb.close();
    secdb.close();
}
