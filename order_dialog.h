//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ORDER_DIALOG_H
#define ORDER_DIALOG_H

#include <QObject>
#include <QDialog>
#include <QLabel>
#include <QGridLayout>
#include <QSpinBox>
#include <QComboBox>
#include <QPushButton>
#include "makeorderwnd.h"

class product;
class makeOrderWnd;
class order_dialog : public QDialog
{
    Q_OBJECT
public:
    explicit order_dialog(product _product, makeOrderWnd *parent = 0);

    QTranslator *translator;
    QString lang;

    QLabel *meal;
    QLabel *size;
    QGridLayout *layout;
    QComboBox *box;

    QComboBox *sortBox;
    QLabel *sortLB;

    QString name;
    QPushButton *okBtn;
    QSpinBox *quantity;

signals:
    void send_product(QString&, QString, int, QString);
public slots:
    void onOkBtnClick();
};


#endif // ORDER_DIALOG_H
