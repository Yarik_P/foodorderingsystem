//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef USER_INPUT_H
#define USER_INPUT_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QCompleter>
#include <QCryptographicHash>
#include <QMessageBox>
#include "makeorderwnd.h"

class month_stat;
class user_input : public QWidget
{
    Q_OBJECT
public:
    explicit user_input();
    QTranslator *translator;
    QString lang;

    QGridLayout *layout;

    QPushButton *mkOrder;

    QLabel *telefonLB;
    QLabel *kundeNrLB;
    QLabel *nameLB;
    QLabel *strasseLB;
    QLabel *strasseLBslash;
    QLabel *bemerkungLB;
    QLabel *hinweisLB;
    QLabel *cityLB;
    //QLabel *postalCodeLB:


    QLineEdit * telefonLE;
    QLineEdit *userIdLE;
    QLineEdit *nameLE;
    QLineEdit *strasseLE1;
    QLineEdit *strasseLE2;
    QLineEdit *strasseLE3;
    QLineEdit *commentLE;
    QLineEdit *noteLE;
    QLineEdit *cityLE;
    QLineEdit *postalCodeLE;

    QComboBox *orderType;

    QString userID;
    QStringList phones;
    QStringList names;
    QString lastName;
    QString lastPhone;

    QSqlDatabase sdb;
    month_stat *q;

    QStringList streets;

public slots:
    void orderBtnClicked();
    void telefonChanged();
    void keyPressEvent(QKeyEvent *event);
    void orderTypeChanged(QString text);

    void cityChanged();
    void nameChanged();
    void streetChanged();
};

#endif // USER_INPUT_H
