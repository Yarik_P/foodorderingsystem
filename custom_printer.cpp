#include "custom_printer.h"

custom_printer::custom_printer()
{

}

void custom_printer::print_order(QMap<QString, QString> orders, shop ThisShop, QString &username, QString &address, QString &post_code, QString &city, QString &user_phone)
{/*
    QPrinter printer;
    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle(tr("Print Document"));

    if (dialog->exec() != QDialog::Accepted) {
        return;
    }

    QPainter painter;
    painter.begin(&printer);

    // Draw Header
    QString text = ThisShop.name + " | " + ThisShop.street + " | " + ThisShop.postal_code + " " + ThisShop.city;
    int offset = 210;
    painter.drawText(100, 50, 350, 100, Qt::AlignLeft|Qt::AlignTop, text);
    painter.drawText(100, 70, 350, 100, Qt::AlignLeft|Qt::AlignTop, username);
    painter.drawText(100, 90, 600, 100, Qt::AlignLeft|Qt::AlignTop, address);
    painter.drawText(100, 110, 350, 100, Qt::AlignLeft|Qt::AlignTop, post_code + " " + city);
    painter.drawText(100, 110, 350, 100, Qt::AlignRight|Qt::AlignTop, QDateTime::currentDateTime().toString("hh:mm dd.MM.yyyy"));
    painter.drawText(100, 130, 350, 100, Qt::AlignLeft|Qt::AlignTop, tr("Tel: ") + user_phone);


    text = tr("Quantity");
    painter.drawText(75, offset - 50, 75, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
    painter.drawRect(75, offset - 50, 75, 50);

    text = tr("Name");
    painter.drawText(150, offset - 50, 465, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
    painter.drawRect(150, offset - 50, 470, 50);

    text = tr("Summ/€");

    painter.drawText(620, offset - 50, 100, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
    painter.drawRect(620, offset - 50, 100, 50);

    int i = 0;

    for (i = 0; i < orders.count(); i++){
        QString text = orders[i];
        qDebug() << text;

        text = orderTB->item(i, 2)->text();
        painter.drawText(75, 50 * i + offset, 75, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
        painter.drawRect(75, 50 * i + offset, 75, 50);

        //rect is table border, smrect is smaller invisible rectangle to hold text
        text = orderTB->item(i, 1)->text();
        QRectF *rect = new QRectF(150, 50 * i + offset, 470, 50);
        painter.drawRect(*rect);

        if (text.length() > 67){
            QRectF *smRect = new QRectF(155, 50 * i + offset + 3, 460, 50);
            painter.drawText(*smRect, text);
        } else {
            painter.drawText(155, 50 * i + offset + 15, 460, 50, Qt::AlignLeft, text);
        }

        // summ
        text = orderTB->item(i, 3)->text() + " EUR";
        painter.drawText(620, 50 * i + offset, 100, 50, Qt::AlignHCenter|Qt::AlignCenter, text);
        painter.drawRect(620, 50 * i + offset, 100, 50);
    }
    painter.drawRect(75, 50 * i + offset, 645, 50);
    painter.drawText(75, 50 * i + offset, 640, 50, Qt::AlignCenter|Qt::AlignRight, tr("MwSt. (7%): ") + this->mwstTE->text());
    i++;
    painter.drawRect(75, 50 * i + offset, 645, 50);
    painter.drawText(75, 50 * i + offset, 640, 50, Qt::AlignCenter|Qt::AlignRight, tr("MwSt. (19%): ") + this->mwst19TE->text());
    i++;
    painter.drawRect(75, 50 * i + offset, 645, 50);
    painter.drawText(75, 50 * i + offset, 640, 50, Qt::AlignCenter|Qt::AlignRight, tr("Invoice amount: ") + QString::number(this->finalSum));

    painter.end();*/
}
