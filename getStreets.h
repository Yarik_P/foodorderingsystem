//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GETSTREETSWND_H
#define GETSTREETSWND_H

#include <QWidget>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QDebug>
#include <QFile>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QRegularExpression>


class getStreetsWnd : public QWidget
{
    Q_OBJECT

public:
    getStreetsWnd(QWidget *parent = 0);
    ~getStreetsWnd();
    QGridLayout *layout;
    QLineEdit *strNameLE;
    QLabel *workLabel;
    QPushButton *okBtn;

    QNetworkAccessManager *manager;
    QString cityName;
public slots:
    void replyFinished(QNetworkReply * reply);
    void okBtnClicked();
};

#endif // GETSTREETSWND_H
