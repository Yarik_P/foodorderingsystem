//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXPENDITURESWND_H
#define EXPENDITURESWND_H

#include <QObject>
#include <QWidget>
#include <QtSql>
#include <QDateEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QDebug>
#include <QComboBox>
#include <QLabel>

#include "expenditurewidget.h"

class expendituresWnd : public QWidget
{
    Q_OBJECT
public:
    explicit expendituresWnd();
    QLabel *mainLB;

    QLabel *productsLB;
    QLabel *businessLB;
    QLabel *privateLB;
    QLabel *otherLB;

    QTextEdit *productsTE;
    QTextEdit *businessTE;
    QTextEdit *privateTE;
    QTextEdit *otherTE;

    QLineEdit *productsSumLE;
    QLineEdit *businessSumLE;
    QLineEdit *privateSumLE;
    QLineEdit *otherSumLE;

    QGridLayout *layout;

    QLabel *dateLB;
    QDateEdit *dateE;

    QPushButton *okBtn;
    QLocale *german = new QLocale(QLocale::German, QLocale::Germany);

    int rows = 4;

    QPushButton *addExp;

    QList<expenditureWidget*> wdgts;

    bool higher;

public slots:
    void populate();
    void dateChanged(QDate q);

    void addExpClicked();
    void deleteExp();
};

#endif // EXPENDITURESWND_H
