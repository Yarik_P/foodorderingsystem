//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MAKEORDERWND_H
#define MAKEORDERWND_H


#include <QtPrintSupport/QPrintDialog>
#include <QPainter>
#include <QtPrintSupport/QPrinter>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QCryptographicHash>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QByteArray>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QString>
#include <QLineEdit>
#include <QTableWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QDebug>
#include <QEventLoop>
#include <QModelIndex>
#include <QHeaderView>
#include <QList>
#include <QFontMetrics>
#include <QFont>
#include <QMap>
#include <QDialog>
#include <QKeyEvent>
#include <QtSql>
#include <QMessageBox>
#include <QComboBox>

#include "product.h"
#include "order_dialog.h"

class orderWnd;
class size;
class shop;
class offer;
class product;

class makeOrderWnd : public QWidget
{
    Q_OBJECT
public:
    explicit makeOrderWnd(QString order = "", QString orderTypeStr = "");

    int mealsSearchPresizion = 0;
    int addingsSearchPresizion = 0;
    QString orderTypeStr;


    QPushButton *searchNext;
    QPushButton *searchPrevious;

    QPushButton *addSearchNext;
    QPushButton *addSearchPrevious;

    bool getOrder = false;

    QTranslator *translator;
    QString lang;

    QGridLayout *orderLay;
    QGroupBox *orderGB;
    QTableWidget* orderTB;

    QString userId;
    QLabel *summL;
    QLineEdit *summTE;
    float summ = 0;
    QLabel *addittionsSummL;
    QLineEdit *addittionsSummTE;
    float additionsSumm = 0;
    QLabel *deliveryL;
    QLineEdit *deliveryTE;
    float deliverySum = 0;
    QLabel *mwstL;
    QLineEdit *mwstTE;
    float mwst = 0;
    QLabel *mwst19L;
    QLineEdit *mwst19TE;
    float mwst19 = 0;
    QLabel *discountL;
    QLineEdit *discountTE;
    QLabel *finalSumL;
    QLineEdit *finalSumTE;
    float finalSum = 0;


    QGridLayout *mealsLay;
    QGroupBox *mealsGB;
    QTableWidget *mealsTB;
    QGridLayout *additionsLay;
    QGroupBox *additionsGB;
    QTableWidget *additionsTB;

    QLabel *productsSearchL;
    QLineEdit *productsSearchLE;
    QLabel *addingsSearchL;
    QLineEdit *addingsSearchLE;


    QPushButton *printBtn;

    QGridLayout *layout;

    QGroupBox *userInfoGB;
    QGridLayout *userInfoLay;

    QList<product> meals;
    QList<product> additions;
    QList<category *> categories;

    offer activeOffer;

    QNetworkAccessManager *manager;
    QNetworkReply *replyer;
    QByteArray datas;
    void remove_from_order();
    product get_product(QString name);
    void update_order();

    category *get_category(int id);

    ~makeOrderWnd();
    QString orderJson;

    QComboBox *orderType;
    QString username;
    QString address;
    QString city;
    QString post_code;
    QString user_phone;

    QString order_uid;

    shop *ThisShop;
    QString orderStr;
    QLocale german;
    QComboBox *shopidCB;
    static void print_order(QMap<QString, int> orders, shop ThisShop, user Usr, QString mwst7str, QString mwst19str, QString sumStr, bool showInfo);
    static void print_order2(QMap<QString, int> orders, shop ThisShop, QString sumStr, int orderCounter);

    int shopid = -1;
    int setid = -1;
signals:

public slots:
    // void onResult(QNetworkReply* reply);
    void setProduct(QTableWidget *table, product prod);
    void OnTableDoubleclicked();
    void add_to_order(QString &_prod, QString size, int _quantity, QString sort="");
    void OnAddingsTableDoubleclicked();
    void meals_search(QString text);
    void replying();
    void keyPressEvent(QKeyEvent *event);
    void deliveryCostChanged(QString text);
    void getProductSize(product _product);
    void mealsTBClicked(QModelIndex ind);
    void printBtnClicked();

    void mealSearchNext();
    void mealSearchBack();
    void additionsSearchBack();
    void additionsSearchNext();


    void orderTypeChanged(QString text);

    void shopidChanged(QString q);

};

#endif // MAKEORDERWND_H
