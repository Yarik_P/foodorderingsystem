#include "month_stat.h"

#include "iostream"

month_stat::month_stat(QWidget *parent) : QWidget(parent)
{
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
          qDebug() << sdb.lastError().text();
    }
    sdb.close();
    this->setWindowTitle("Month order");
    layout = new QGridLayout(this);

    globalGB = new QGroupBox("Month overview", this);
    globalGBlayout = new QGridLayout(globalGB);

    firstGB = new QGroupBox(globalGB);
    thirdGB = new QGroupBox(globalGB);
    mwstGB = new QGroupBox(globalGB);

    firstGBLayout = new QGridLayout(firstGB);
    thirdGBLayout = new QGridLayout(thirdGB);
    mwstGBLayout = new QGridLayout(mwstGB);

    sumLB = new QLabel(tr("sum"), this);
    ordersLB = new QLabel(tr("orders"), this);
    cashLB = new QLabel(tr("Cash"), this);
    mealsLB = new QLabel(tr("Sellings"), this);
    deliveryLB = new QLabel(tr("Delivery"), this);
    otherLB = new QLabel(tr("Additionally"), this);

    mwst7LB = new QLabel(tr("MwSt 7%"), this);
    mwst19LB = new QLabel(tr("MwSt 19%"), this);
    mwst7LE = new QLineEdit(this);
    mwst19LE = new QLineEdit(this);
    mwst7LE->setReadOnly(true);
    mwst19LE->setReadOnly(true);


    firstGBLayout->addWidget(sumLB, 1, 1);
    firstGBLayout->addWidget(ordersLB, 2, 1);

    thirdGBLayout->addWidget(cashLB, 1, 1);
    thirdGBLayout->addWidget(mealsLB, 2, 1);
    thirdGBLayout->addWidget(deliveryLB, 3, 1);
    thirdGBLayout->addWidget(otherLB, 4, 1);

    mwstGBLayout->addWidget(mwst7LB, 1, 1);
    mwstGBLayout->addWidget(mwst7LE, 1, 2);
    mwstGBLayout->addWidget(mwst19LB, 2, 1);
    mwstGBLayout->addWidget(mwst19LE, 2, 2);

    monthCB = new QComboBox(this);
    monthCB->addItem(tr("All"));
    for (int i = 1; i <= 12; i++) {
        QString str = QString::number(i);
        if (str.length() == 1) {
            str.insert(0, "0");
        }
        monthCB->addItem(str);
    }
    globalGBlayout->addWidget(monthCB, 1, 2);

    yearCB = new QComboBox(this);


    if (!sdb.open()) {
          qDebug() << sdb.lastError();
    }

    QString getYearQuery = "SELECT DISTINCT strftime('%Y', date) y FROM `orders`;";
    QSqlQuery query(getYearQuery);

    while (query.next()) {
        QString value = query.value(0).toString();
        yearCB->addItem(value);
    }
    globalGBlayout->addWidget(yearCB, 1, 3);


    sumLE = new QLineEdit(this);
    ordersLE = new QLineEdit(this);
    sumLE->setReadOnly(true);
    ordersLE->setReadOnly(true);

    cashLE = new QLineEdit(this);
    mealsLE = new QLineEdit(this);
    deliveryLE = new QLineEdit(this);
    otherLE = new QLineEdit(this);
    cashLE->setReadOnly(true);
    mealsLE->setReadOnly(true);
    deliveryLE->setReadOnly(true);
    otherLE->setReadOnly(true);


    firstGBLayout->addWidget(sumLE, 1, 2);
    firstGBLayout->addWidget(ordersLE, 2, 2);


    thirdGBLayout->addWidget(cashLE, 1, 2);
    thirdGBLayout->addWidget(mealsLE, 2, 2);
    thirdGBLayout->addWidget(deliveryLE, 3, 2);
    thirdGBLayout->addWidget(otherLE, 4, 2);


    firstGB->setLayout(firstGBLayout);
    thirdGB->setLayout(thirdGBLayout);
    mwstGB->setLayout(mwstGBLayout);

    globalGBlayout->addWidget(firstGB, 2, 2, 1, 2);
    globalGBlayout->addWidget(thirdGB, 4, 2, 1, 2);
    globalGBlayout->addWidget(mwstGB, 5, 2, 1, 2);


    expendituresLB = new QLabel(tr("Expenditures"));
    expendituresLE = new QLineEdit(this);
    expendituresLE->setReadOnly(true);

    profitLB = new QLabel(tr("Profit"));
    profitLE = new QLineEdit(this);
    profitLE->setReadOnly(true);

    globalGBlayout->addWidget(expendituresLB, 6, 2);
    globalGBlayout->addWidget(expendituresLE, 6, 3);
    globalGBlayout->addWidget(profitLB, 7, 2);
    globalGBlayout->addWidget(profitLE, 7, 3);


    globalGB->setLayout(globalGBlayout);
    layout->addWidget(globalGB);


    updateData();
    connect(yearCB, SIGNAL(currentTextChanged(QString)), this, SLOT(updateData()));
    connect(monthCB, SIGNAL(currentTextChanged(QString)), this, SLOT(updateData()));
    this->setLayout(layout);
}

void month_stat::updateData()
{
    // updates all the line edits
    if (!sdb.open()) {
          qDebug() << sdb.lastError();
    }
    QString str;
    QString str1;

    if (monthCB->currentText() == tr("All")) {
        str = QString("select sum(summ) as summ, count(`_rowid_`) as counter, sum(mwst7) as mwst, sum(mwst19) as mwst19 from orders where `date` "
                      "between '%1-01-01' and '%1-12-31'").arg(yearCB->currentText());
        str1 = QString("select sum(`summ`) as summ from expenditures2 where `date` between '%1-01-01' and '%1-12-31'"
                       ).arg(yearCB->currentText());
    } else {
        str = QString("select sum(summ) as summ, count(`_rowid_`) as counter, sum(mwst7) as mwst, sum(mwst19) as mwst19 from orders where `date` "
                      "between '%1-%2-01' and '%1-%2-31'").arg(yearCB->currentText()).arg(monthCB->currentText());
        str1 = QString("select sum(`summ`) as summ from expenditures2 where `date` between '%1-%2-01' and '%1-%2-31'"
                       ).arg(yearCB->currentText()).arg(monthCB->currentText());
    }

    QSqlQuery query(str);
    int idSumm = query.record().indexOf("summ");
    int idCounter = query.record().indexOf("counter");
    int idmwst = query.record().indexOf("mwst");
    int idmwst19 = query.record().indexOf("mwst19");

    QSqlQuery query1(str1);
    int idSumm1 = query1.record().indexOf("summ");
    query.next();
    query1.next();
    float expendituresSumm = query1.value(idSumm1).toFloat();

    float value = query.value(idSumm).toFloat();

    sumLE->setText((german->toString(value, 'f', 2)));
    cashLE->setText((german->toString(value, 'f', 2)));
    ordersLE->setText(query.value(idCounter).toString());
    mwst7LE->setText((german->toString(query.value(idmwst).toFloat(), 'f', 2)));
    mwst19LE->setText((german->toString(query.value(idmwst19).toFloat(), 'f', 2)));
    expendituresLE->setText(german->toString(expendituresSumm, 'f', 2));
    profitLE->setText(german->toString(value - expendituresSumm, 'f', 2));
    sdb.close();
}

