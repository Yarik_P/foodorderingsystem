//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PRODUCT_H
#define PRODUCT_H
#include <QString>
#include <QList>
#include <QDateTime>
#include <QMap>

class category;
class topping;

class product
{
public:
    product(category *_cat, int _id, QString _code, QString _name);
    category *cat;
    int category_id;
    int id;
    QString code;
    QString name;
    QMap<QString, float> prices;
};


class driver
{
public:
    driver(QString _code, QString _name, QString _telephone);
    QString code;
    QString name;
    QString telephone;
};

class topping {
public:
    topping(int _id, QString _name, int _category_id);
    int id;
    QString name;
    int category_id;
    QMap<int, float> prices;
};


class category
{
public:
    category(int _id, QString _name, int _tax);
    int id;
    QString name;
    bool tax_7;
    QList<topping*> category_toppings;
    QMap<int, QString> sizes;
    QStringList sorts;
};

class shop
{
public:
    shop(int _id, QString _name, QString _code, QString _street, QString _postal_code, QString _city, QString _telephone, QString _fax,
         QString _tax_number);
    int id;
    QString name;
    QString code;
    QString street;
    QString postal_code;
    QString city;
    QString telephone;
    QString fax;
    QString tax_number;
    QList<driver*> drivers;
};

class user
{
public:
    user(QString username, QString address, QString post_code, QString city, QString user_phone);
    QString username;
    QString address;
    QString post_code;
    QString city;
    QString user_phone;

};

class offer
{
public:
    offer();
    int category_id;
    int size_id;
    float price;
};

#endif // PRODUCT_H
