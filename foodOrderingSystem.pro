#-------------------------------------------------
#
# Project created by QtCreator 2017-03-23T15:03:56
#
#-------------------------------------------------

include(xlsx/qtxlsx.pri)

QT += core
QT += gui
QT += network
QT += script
QT += printsupport
QT += sql


win32:RC_ICONS += pizza.ico
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = azzip
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    product.cpp \
    order_dialog.cpp \
    makeorderwnd.cpp \
    user_input.cpp \
    reportswnd.cpp \
    month_stat.cpp \
    orderswnd.cpp \
    setdriverwnd.cpp \
    expenditureswnd.cpp \
    dailystatwnd.cpp \
    expenditurewidget.cpp \
    backupwnd.cpp \
    getStreets.cpp \
    userslistwnd.cpp

HEADERS  += mainwindow.h \
    product.h \
    order_dialog.h \
    makeorderwnd.h \
    user_input.h \
    reportswnd.h \
    month_stat.h \
    orderswnd.h \
    setdriverwnd.h \
    expenditureswnd.h \
    dailystatwnd.h \
    expenditurewidget.h \
    backupwnd.h \
    getStreets.h \
    userslistwnd.h

TRANSLATIONS = languages/en.ts  languages/de.ts

DISTFILES += pizza.ico


