#include "setdriverwnd.h"

setDriverWnd::setDriverWnd(QWidget *parent) : QDialog(parent)
{
    this->resize(150, 150);
    this->setWindowTitle(tr("Choose driver"));
    layout = new QGridLayout(this);
    driversCB = new QComboBox(this);
    layout->addWidget(driversCB, 1, 1);

    okBtn = new QPushButton(tr("Ok"));
    layout->addWidget(okBtn, 2, 1);

    this->setLayout(layout);
    connect(okBtn, SIGNAL(clicked()), this, SLOT(onOkBtnClicked()));
}

void setDriverWnd::onOkBtnClicked() {
    driver = driversCB->currentText();
    this->hide();
}
