//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXPENDITUREWIDGET_H
#define EXPENDITUREWIDGET_H

#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QtSql>
#include <QMessageBox>
#include <QString>
#include <QCryptographicHash>

class expenditureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit expenditureWidget(bool haveValue = false);

    bool created;

    int rowid;
    QComboBox *expTypeCB;
    QHBoxLayout *layout;
    QLineEdit *expTextLE;
    QLineEdit *billAmountLE;
    QLineEdit *taxAmount;
    QPushButton *deleteBtn;
    QPushButton *editBtn;
    bool edited;
    void setEnabled(bool enabled);

    QString expDate;
signals:
    void deleteExpSignal();

public slots:
    void updateExp();
    void deleteExp();

};

#endif // EXPENDITUREWIDGET_H
