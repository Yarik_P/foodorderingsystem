#include "product.h"

product::product(category *_cat, int _id, QString _code, QString _name)
{
    this->cat = _cat;
    this->category_id = cat->id;
    this->id = _id;
    this->code = _code;
    this->name = _name;
}

category::category(int _id, QString _name, int _tax)
{
    this->id = _id;
    this->name = _name;
    this->tax_7 = (_tax == 7);
}

topping::topping(int _id, QString _name, int _category_id)
{
    this->id = _id;
    this->name = _name;
    this->category_id = _category_id;
}

driver::driver(QString _code, QString _name, QString _telephone)
{
    this->code = _code;
    this->name = _name;
    this->telephone = _telephone;
}

shop::shop(int _id, QString _name, QString _code, QString _street, QString _postal_code, QString _city, QString _telephone, QString _fax,
           QString _tax_number)
{
    this->id = _id;
    this->name = _name;
    this->code = _code;
    this->street = _street;
    this->postal_code = _postal_code;
    this->city = _city;
    this->telephone = _telephone;
    this->fax = _fax;
    this->tax_number = _tax_number;
}


user::user(QString username, QString address, QString post_code, QString city, QString user_phone)
{
    this->username = username;
    this->address = address;
    this->post_code = post_code;
    this->city = city;
    this->user_phone = user_phone;
}

offer::offer()
{

}
