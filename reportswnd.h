//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef REPORTSWND_H
#define REPORTSWND_H

#include <QObject>
#include <QWidget>
#include <QDateEdit>
#include <QGridLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include <QLabel>
#include <QDebug>
#include <QtSql>
#include <QMessageBox>

#include "xlsxdocument.h"

#include "month_stat.h"
#include "dailystatwnd.h"



class reportsWnd : public QWidget
{
    Q_OBJECT
public:
    explicit reportsWnd(QWidget *parent = 0);


    QPushButton *monthStat;
    QGridLayout *layout;
    QPushButton *printBtn;
    QPushButton *someBtn;
    QPushButton *dailyReportBtn;
    QComboBox *driverCB;
    QLabel *dateBegin;
    QLabel *dateEnd;
    QTableWidget *statTable;

    QDateEdit *dateBeginDE;
    QDateEdit *dateEndDE;
    month_stat *q;
    dailyStatWnd *z;
    QString normalizeNumber(QString number);
    QSqlDatabase sdb;

    QSqlQuery query;


public slots:
    void populateTable(QString _query = "");
    void searchButtonClicked();
    void monthStatClicked();
    void driverChanged(QString str);
    void dailyReportBtnClicked();
    void printBtnClicked();
};

#endif // REPORTSWND_H
