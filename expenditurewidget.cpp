#include "expenditurewidget.h"

expenditureWidget::expenditureWidget(bool haveValue) : QWidget()
{
    created = haveValue;
    layout = new QHBoxLayout(this);

    expTypeCB = new QComboBox(this);
    expTypeCB->addItem(tr("Products"), 0);
    expTypeCB->addItem(tr("Business"), 1);
    expTypeCB->addItem(tr("Private"), 2);
    expTypeCB->addItem(tr("Other"), 3);
    layout->addWidget(expTypeCB);
    layout->addSpacing(20);

    expTextLE = new QLineEdit(this);
    expTextLE->setPlaceholderText(tr("Ependiture text"));
    layout->addWidget(expTextLE);

    billAmountLE = new QLineEdit(this);
    billAmountLE->setPlaceholderText(tr("Expenditure cost"));
    layout->addWidget(billAmountLE);


    taxAmount = new QLineEdit(this);
    taxAmount->setPlaceholderText(tr("Expenditure MwSt"));
    layout->addWidget(taxAmount);


    editBtn = new QPushButton(tr("Save"), this);
    layout->addWidget(editBtn);

    deleteBtn = new QPushButton(tr("Delete"), this);
    layout->addWidget(deleteBtn);

    connect(editBtn, SIGNAL(clicked()), this, SLOT(updateExp()));
    connect(deleteBtn, SIGNAL(clicked()), this, SLOT(deleteExp()));
}

void expenditureWidget::setEnabled(bool enabled)
{
//  functions that "enables" or disables widget
//  some of child widgets sets to readonly, other to disabled state

    editBtn->setEnabled(enabled);
    deleteBtn->setEnabled(enabled);
    expTextLE->setReadOnly(!enabled);
    billAmountLE->setReadOnly(!enabled);
    taxAmount->setReadOnly(!enabled);
    expTypeCB->setEnabled(enabled);
}


void expenditureWidget::updateExp()
{
    QLocale german(QLocale::German, QLocale::Germany);
    if (!edited) {
        this->setEnabled(true);
        edited = !edited;
        editBtn->setText(tr("Save"));
        return;
    }
    float tax = taxAmount->text().toFloat();
    if (tax == 0) {
        tax = german.toFloat(taxAmount->text());
    }
    float bill = billAmountLE->text().toFloat();
    if (bill == 0) {
        bill = german.toFloat(billAmountLE->text());
    }

    if (tax == 0 || bill == 0) {
        QMessageBox::information(this, tr("Error!"), "Tax and bill amounts must be numbers");
        return;
    }

    if(created) {
        QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
        sdb.setDatabaseName("azzip.db");
        if (!sdb.open()) {
            qDebug() << sdb.lastError().text();
        }

        QSqlQuery a_query;

        a_query.prepare("update expenditures2 set `text`=:text, `summ`=:summ, `tax`=:tax, `type`=:type where `_rowid_`=:rowid;");
        a_query.bindValue(":text", expTextLE->text());
        a_query.bindValue(":summ", bill);
        a_query.bindValue(":tax", tax);
        a_query.bindValue(":type", expTypeCB->currentIndex());
        a_query.bindValue(":rowid", rowid);
        a_query.exec();

        sdb.close();
    } else {
        if (expTextLE->text().length() > 0 && billAmountLE->text() > 0 && taxAmount->text() > 0){
            QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
            sdb.setDatabaseName("azzip.db");
            if (!sdb.open()) {
                qDebug() << sdb.lastError().text();
            }

            QSqlQuery a_query;

            a_query.prepare("INSERT INTO `expenditures2`(`date`,`text`,`summ`,`tax`,`type`,`_rowid_`,`exp_uid`) VALUES (:date,:text,:summ,:tax,"
                            ":type,:rowid,:uid);");
            a_query.bindValue(":date", expDate);
            a_query.bindValue(":text", expTextLE->text());
            a_query.bindValue(":summ", bill);
            a_query.bindValue(":tax", tax);
            a_query.bindValue(":type", expTypeCB->currentIndex());
            a_query.bindValue(":rowid", rowid + 1);
            QString toCrypt = expTextLE->text() + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz");
            a_query.bindValue(":uid", QCryptographicHash::hash(toCrypt.toUtf8(),QCryptographicHash::Md5).toHex());
            if(!a_query.exec()) {
                QString toCrypt = expTextLE->text() + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz");
                a_query.bindValue(":uid", QCryptographicHash::hash(toCrypt.toUtf8(),QCryptographicHash::Md5).toHex());
                a_query.exec();
            }
            created = !created;
            sdb.close();
        }
    }
    this->setEnabled(false);
    edited = !edited;
    editBtn->setText(tr("Edit"));
    editBtn->setEnabled(true);
}

void expenditureWidget::deleteExp()
{
    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
        qDebug() << sdb.lastError().text();
    }
    QSqlQuery a_query;
    a_query.prepare("DELETE FROM `expenditures2` WHERE `_rowid_`=:rowid;");
    a_query.bindValue(":rowid", rowid);
    a_query.exec();
    sdb.close();
    deleteExpSignal();
}
