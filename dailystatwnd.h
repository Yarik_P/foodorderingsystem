//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DAILYSTATWND_H
#define DAILYSTATWND_H

#include <QObject>
#include <QWidget>
#include <QtPrintSupport/QPrintDialog>
#include <QPainter>
#include <QPrinter>
#include <QDebug>
#include <QDate>
#include <QtSql>
#include <QtPrintSupport/QPrinter>


#include <QDateEdit>
#include <QGridLayout>
#include <QPushButton>

class dailyStatWnd : public QWidget
{
    Q_OBJECT
public:
    explicit dailyStatWnd(QWidget *parent = 0);
    QPushButton *okBtn;
    QDateEdit *DE;

    QLocale *german = new QLocale(QLocale::German, QLocale::Germany);
signals:

public slots:
    void onOkBtnClicked();
};

#endif // DAILYSTATWND_H
