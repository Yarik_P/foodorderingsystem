#include "expenditureswnd.h"

expendituresWnd::expendituresWnd() : QWidget(0)
{
    this->setWindowTitle(tr("Expenditures"));
    this->resize(800, 150);
    mainLB = new QLabel(tr("Expenditures"));

    layout = new QGridLayout(this);

    layout->addWidget(mainLB, 1, 1, 1, 6, Qt::AlignCenter);

    dateLB = new QLabel(tr("Date"));
    dateE = new QDateEdit(QDate::currentDate(), this);
    dateE->setCalendarPopup(true);
    layout->addWidget(dateLB, 2, 1);
    layout->addWidget(dateE, 2, 2);

    connect(dateE, SIGNAL(dateChanged(QDate)), this, SLOT(dateChanged(QDate)));

    populate();

    addExp = new QPushButton(tr("Add new"));
    //hope there won't be  more than 500 expenditures per day - no screen can show that amount
    layout->addWidget(addExp, 500, 3);
    connect(addExp, SIGNAL(clicked(bool)), this, SLOT(addExpClicked()));
    this->setLayout(layout);
}

void expendituresWnd::populate()
{
    /* function that creates expenditures widgets for every expenditure of chosen day in database */
    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
          qDebug() << sdb.lastError().text();
    }

    QSqlQuery a_query("SELECT _rowid_, * FROM 'expenditures2' where `date`='" + dateE->date().toString("yyyy-MM-dd") + "';");

    a_query.exec();
    while(a_query.next()) {
        int idRowid = a_query.record().indexOf("rowid");
        int idSumm = a_query.record().indexOf("summ");
        int idTax = a_query.record().indexOf("tax");
        int idType = a_query.record().indexOf("type");
        int idText = a_query.record().indexOf("text");
        rows += 1;

        expenditureWidget *qwe = new expenditureWidget(true);
        connect(qwe, SIGNAL(deleteExpSignal()), this, SLOT(deleteExp()));
        wdgts.append(qwe);
        qwe->expTextLE->setText(a_query.value(idText).toString());
        qwe->billAmountLE->setText(german->toString(a_query.value(idSumm).toFloat(), 'f', 2));
        qwe->taxAmount->setText(german->toString(a_query.value(idTax).toFloat(), 'f', 2));
        qwe->expTypeCB->setCurrentIndex(a_query.value(idType).toInt());

        qwe->rowid = a_query.value(idRowid).toInt();
        qwe->edited = false;
        qwe->setEnabled(!this->higher && qwe->edited);
        qwe->editBtn->setEnabled(!this->higher);
        qwe->editBtn->setText(tr("Edit"));
        layout->addWidget(qwe, rows, 1, 1, 6);
    }
    sdb.close();
}

void expendituresWnd::dateChanged(QDate q)
{
//    slot that holds data changing and verifies that user can edit expenditures
//    user can't edit expenditures if they are older then 60 days and can't add expenditures to the future

    QDate newq = q.addDays(60);
    this->higher = !(newq >= QDate::currentDate());
    if (q > QDate::currentDate()){
        this->higher = true;
    }
    addExp->setEnabled(!this->higher);

    for (int i = 0; i < wdgts.length(); i++){
        layout->removeWidget(wdgts.at(i));
        wdgts.at(i)->deleteLater();
        rows -= 1;
    }
    wdgts.clear();
    this->resize(800, 150);
    populate();
}

void expendituresWnd::addExpClicked()
{
//  slot that creates new expenditure Widget to add new expenditure
    expenditureWidget *qwe = new expenditureWidget();
    connect(qwe, SIGNAL(deleteExpSignal()), this, SLOT(deleteExp()));
    wdgts.append(qwe);
    rows += 1;
    qwe->expDate = dateE->date().toString("yyyy-MM-dd");
    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");

    if (!sdb.open()) {
          qDebug() << sdb.lastError().text();
    }

    QSqlQuery a_query("SELECT max(_rowid_) as mx FROM 'expenditures2' where 1;");
    a_query.exec();
    a_query.next();
    qwe->rowid = a_query.value(0).toInt();
    qwe->edited = true;
    layout->addWidget(qwe, rows, 1, 1, 6);
    sdb.close();
}

void expendituresWnd::deleteExp()
{
    dateChanged(dateE->date());
}


