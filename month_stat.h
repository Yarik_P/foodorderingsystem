//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MONTH_STAT_H
#define MONTH_STAT_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QGroupBox>
#include <QPushButton>
#include <QDebug>
#include <QLabel>
#include <QLocale>
#include <QGridLayout>
#include <QtSql>

class month_stat : public QWidget
{
    Q_OBJECT
public:
    explicit month_stat(QWidget *parent = 0);

    QLocale *german = new QLocale(QLocale::German, QLocale::Germany);
    QTranslator *translator;
    QString lang;

    QSqlDatabase sdb;
    QGridLayout *layout;

    QGroupBox *globalGB;
    QGridLayout *globalGBlayout;

    QGroupBox *firstGB;
    QGridLayout *firstGBLayout;

    QGroupBox *thirdGB;
    QGridLayout *thirdGBLayout;

    QGroupBox *mwstGB;
    QGridLayout *mwstGBLayout;

    QLineEdit *mwst7LE;
    QLabel *mwst7LB;
    QLineEdit *mwst19LE;
    QLabel *mwst19LB;


    QComboBox *monthCB;
    QComboBox *yearCB;
    QPushButton *printBtn;

    QLabel *monthLB;
    QLabel *yearLB;
    QLabel *sumLB;
    QLabel *ordersLB;
    QLabel *cashLB;
    QLabel *mealsLB;
    QLabel *deliveryLB;
    QLabel *otherLB;

    QLineEdit *sumLE;
    QLineEdit *ordersLE;
    QLineEdit *cashLE;
    QLineEdit *mealsLE;
    QLineEdit *deliveryLE;
    QLineEdit *otherLE;

    QLabel *expendituresLB;
    QLineEdit *expendituresLE;

    QLabel *profitLB;
    QLineEdit *profitLE;

public slots:
    void updateData();
};

#endif // MONTH_STAT_H
