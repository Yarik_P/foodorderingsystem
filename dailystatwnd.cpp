#include "dailystatwnd.h"


#include "dailystatwnd.h"

dailyStatWnd::dailyStatWnd(QWidget *parent) : QWidget(parent)
{
    this->resize(200, 200);
    this->setWindowTitle(tr("Choose date"));
    QGridLayout *layout = new QGridLayout(this);
    okBtn = new QPushButton("Ok", this);
    layout->addWidget(okBtn, 2, 1);

    this->setLocale(*german);
    DE = new QDateEdit(QDate::currentDate());
    DE->setCalendarPopup(true);

    layout->addWidget(DE, 1, 1);


    this->setLayout(layout);

    connect(okBtn, SIGNAL(clicked()), this, SLOT(onOkBtnClicked()));

}

void dailyStatWnd::onOkBtnClicked()
{
    /* Function that organizes all printer work and inserts data about day into database */
    QPrinter printer;
    QPixmap *background = new QPixmap("images/kassen_stock.png");
    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle(tr("Print Document"));

    if (dialog->exec() != QDialog::Accepted) {
        return;
    }

    QPainter painter;
    painter.begin(&printer);
    painter.drawPixmap(0, 0, 767, 1097, *background);

    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
          qDebug() << sdb.lastError().text();
    }
    QSqlQuery query;
    query.prepare("select sum(tax) as tax, sum(summ) as summ from expenditures2 where date=:date");
    query.bindValue(":date", DE->date().toString("yyyy-MM-dd"));
    query.exec();
    query.next();

    float sumTax = query.value(0).toFloat();
    float sumMoney = query.value(1).toFloat();
    qDebug() << sumTax << sumMoney;
    QList<int> starts;
    QList<int> summsCoords;
    starts << 400 << 607 << 703 << 747;
    summsCoords << 560 << 653 << 699 << 768;

    QList<float> summs;
    float oneSum;
    QRectF *smRect;
    painter.drawText(180, 315, DE->date().toString("dd-MM-yyyy"));
    for (int i = 0; i < 4; i++) {
        query.exec("select * from expenditures2 where date='" + DE->date().toString("yyyy-MM-dd") + "' and type=" + QString::number(i) + ";");
        oneSum = 0;
        while (query.next()) {
            int idText = query.record().indexOf("text");
            int idSumm = query.record().indexOf("summ");
            int idTax =  query.record().indexOf("tax");

            int j = 0;
            smRect = new QRectF(108, starts.at(i) + j * 22, 300, 21);
            painter.drawText(*smRect, query.value(idText).toString());

            smRect = new QRectF(330, starts.at(i) + j * 22, 300, 21);
            painter.drawText(*smRect, german->toString(query.value(idTax).toFloat(), 'f', 2));

            smRect = new QRectF(428, starts.at(i) + j * 22, 300, 21);
            painter.drawText(*smRect, german->toString(query.value(idSumm).toFloat(), 'f', 2));
            j++;
            oneSum += query.value(idSumm).toFloat();
        }
        smRect = new QRectF(526, summsCoords.at(i), 300, 21);
        painter.drawText(*smRect, german->toString(oneSum));
        summs << oneSum;
    }
    query.exec("select cash_begin from days where date=:date");
    query.bindValue(":date", DE->date().toString("yyyy-MM-dd"));
    query.exec();
    float cash_begin;
    if (query.next()) {
        cash_begin = query.value(0).toDouble();
    } else {
        query.exec("select max(date) as date, cash_end from days where 1;");
        query.next();
        cash_begin = query.value(1).toDouble();
        query.prepare("insert into days (`date`, `cash_begin`) VALUES ('" + DE->date().toString("yyyy-MM-dd") + "'," +
                      QString::number(cash_begin, 'f', 2) + ");");
        if (!query.exec()){
            qDebug() << query.lastError();
        }
    }

    float expSumm = 0;
    foreach (float num, summs) {
        expSumm += num;
    }

    smRect = new QRectF(526, 794, 120, 21);
    painter.drawText(*smRect, german->toString(expSumm, 'f', 2));

    query.prepare("select sum(summ) as summ from orders where date=:date");
    query.bindValue(":date", DE->date().toString("yyyy-MM-dd"));
    query.exec();
    query.next();

    smRect = new QRectF(526, 818, 120, 100);
    float got_summ = query.value(0).toDouble();
    painter.drawText(*smRect, german->toString(got_summ, 'f', 2));

    query.prepare("select cash_begin from days where date=:date");
    query.bindValue(":date", DE->date().toString("yyyy-MM-dd"));
    query.exec();
    query.next();

    float cash_end;

    cash_end = cash_begin - expSumm + got_summ;

    smRect = new QRectF(526, 326, 80, 21);
    painter.drawText(*smRect, german->toString(cash_begin, 'f', 2));

    smRect = new QRectF(526, 840, 120, 100);
    painter.drawText(*smRect, german->toString(cash_end, 'f', 2));

    query.exec("update days set `cash_end`=" + QString::number(cash_end, 'f', 2) + " where date='" + DE->date().toString("yyyy-MM-dd") + "';");

    this->close();
}
