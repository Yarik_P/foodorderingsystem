#include "order_dialog.h"

order_dialog::order_dialog(product _product, makeOrderWnd *parent) : QDialog(parent)
{
    this->setWindowTitle(tr("Choose size and quantity"));
    this->resize(300, 200);
    layout = new QGridLayout(this);
    meal = new QLabel(_product.name);
    quantity = new QSpinBox(this);
    quantity->setMaximum(999);
    quantity->setMinimum(1);
    name = _product.name;
    size = new QLabel(tr("Size: "), this);
    box = new QComboBox(this);
    sortLB = new QLabel(tr("Sort: "), this);
    sortBox = new QComboBox(this);

    if (_product.cat->sizes.values().count() == 1) {
        box->setHidden(true);
        size->setHidden(true);
        box->addItem("");
    } else {
        foreach (QString size, _product.cat->sizes.values()) {
            box->addItem(size);
        }
    }

    if (_product.cat->sorts.length() == 0) {
        sortBox->setHidden(true);
        sortLB->setHidden(true);
        sortBox->addItem("");
    } else {
        foreach (QString str, _product.cat->sorts) {
           sortBox->addItem(str);
        }
    }

    okBtn = new QPushButton(tr("ok"), this);
    layout->addWidget(quantity, 1, 2);
    layout->addWidget(meal, 1, 1);
    layout->addWidget(size, 2, 1);
    layout->addWidget(box, 2, 2);
    layout->addWidget(sortLB, 3, 1);
    layout->addWidget(sortBox, 3, 2);
    layout->addWidget(okBtn, 4, 1, 1, 2);

    this->setLayout(layout);
    connect(this->okBtn, SIGNAL(clicked()), this, SLOT(onOkBtnClick()));
}

void order_dialog::onOkBtnClick()
{
    if (quantity->text().toInt() > 0) {
        emit send_product(name, box->currentText(), quantity->text().toInt(), sortBox->currentText());
        this->close();
    }
}
