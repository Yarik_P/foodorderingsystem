﻿#include "userslistwnd.h"

usersListWnd::usersListWnd(QWidget *parent) : QWidget(parent)
{
    searchTermCB = new QComboBox(this);
    searchTermCB->addItem(tr("Postalcode"));
    searchTermCB->addItem(tr("City"));
    searchTermCB->addItem(tr("Number of orders"));
    searchTermCB->addItem(tr("Last ordered above"));

    searchLine = new QLineEdit(this);
    searchBtn = new QPushButton(tr("Search"), this);

    table = new QTableWidget(0, 9, this);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);

    selectAllCB = new QCheckBox(tr("Select all"), this);
    shopSelectCB = new QComboBox(this);

    summLE = new QLineEdit(this);
    QDoubleValidator *valid = new QDoubleValidator(this);
    valid->setRange(0, 100000, 2);

    summLE->setValidator(valid);
    printBtn = new QPushButton(tr("Print"), this);

    layout = new QGridLayout(this);

    layout->addWidget(searchTermCB, 1, 0);
    layout->addWidget(searchLine, 1, 1);
    layout->addWidget(searchBtn, 1, 2, 1, 2);

    layout->addWidget(selectAllCB, 2, 0);
    layout->addWidget(shopSelectCB, 2, 1);
    layout->addWidget(summLE, 2, 2);
    layout->addWidget(printBtn, 2, 3);

    layout->addWidget(table, 3, 0, 5, 4);

    this->setLayout(layout);
    this->setWindowTitle(tr("Users"));
    this->resize(1100, 400);

    QTableWidgetItem *header = new QTableWidgetItem(tr("User ID"));
    table->setHorizontalHeaderItem(1, header);
    header = new QTableWidgetItem(tr("Select"));
    table->setHorizontalHeaderItem(2, header);
    header = new QTableWidgetItem(tr("Mobile"));
    table->setHorizontalHeaderItem(2, header);
    header = new QTableWidgetItem(tr("Name"));
    table->setHorizontalHeaderItem(3, header);
    header = new QTableWidgetItem(tr("Street"));
    table->setHorizontalHeaderItem(4, header);
    header = new QTableWidgetItem(tr("Order count"));
    table->setHorizontalHeaderItem(5, header);
    header = new QTableWidgetItem(tr("Last ordered at"));
    table->setHorizontalHeaderItem(6, header);
    header = new QTableWidgetItem(tr("City"));
    table->setHorizontalHeaderItem(7, header);
    header = new QTableWidgetItem(tr("Postal code"));
    table->setHorizontalHeaderItem(8, header);

    table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(6, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(7, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(8, QHeaderView::Stretch);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("azzip.db");
    if (!db.open()) {
        qDebug() << "error opening db";
    }
    populateTB();
    connect(searchBtn, SIGNAL(clicked()), this, SLOT(searchBtnClicked()));
    connect(selectAllCB, SIGNAL(toggled(bool)), this, SLOT(selectAllClicked(bool)));
    connect(printBtn, SIGNAL(clicked()), this, SLOT(printBtnClicked()));

    QByteArray datas;
    QFile file("shops.json");
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        datas = stream.readAll().toUtf8();
    }

    QJsonDocument doc = QJsonDocument::fromJson(datas);
    QJsonObject object = doc.object();
    QJsonArray shops = object["shops"].toArray();
    foreach (QJsonValue shopJS, shops) {
        QJsonObject obj = shopJS.toObject();
        int id = obj["id"].toInt();
        QString name = obj["name"].toString();
        QString code = obj["code"].toString();
        QString street = obj["street"].toString();
        QString postal_code = obj["postal_code"].toString();
        QString city = obj["city"].toString();
        QString telephone = obj["telephone"].toString();
        QString fax = obj["fax"].toString();
        QString tax_number = obj["tax_number"].toString();

        shopSelectCB->addItem(name);
        shop *currentShop = new shop(id, name, code, street, postal_code, city, telephone, fax, tax_number);

        shopsList.append(currentShop);
    }
}

usersListWnd::~usersListWnd()
{
    db.close();
}

void usersListWnd::populateTB(QString Query)
{
    // Function that populates table with data according to sql query
    if (Query.length() == 0){
        Query = "SELECT * FROM 'Users' WHERE 1;";
    }
    QSqlQuery query;
    query.exec(Query);
    for (int i = 0; i < table->rowCount(); i++){
        table->removeRow(0);
    }
    table->setRowCount(0);
    while(query.next()){
        table->setRowCount(table->rowCount() + 1);
        int uuid = query.record().indexOf("user_uuid");
        int mobile = query.record().indexOf("mobile");
        int Name = query.record().indexOf("Name");
        int Street = query.record().indexOf("Street");
        int order_count = query.record().indexOf("order_count");
        int last_ordered_at = query.record().indexOf("last_ordered_at");
        int city = query.record().indexOf("city");
        int postcode = query.record().indexOf("postal_code");

        QCheckBox *checked = new QCheckBox(this);
        table->setCellWidget(table->rowCount() - 1, 0, checked);

        QTableWidgetItem *item = new QTableWidgetItem(query.value(uuid).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 1, item);

        item = new QTableWidgetItem(query.value(mobile).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 2, item);

        item = new QTableWidgetItem(query.value(Name).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 3, item);

        item = new QTableWidgetItem(query.value(Street).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 4, item);


        item = new QTableWidgetItem(query.value(order_count).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 5, item);

        item = new QTableWidgetItem(query.value(last_ordered_at).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 6, item);

        item = new QTableWidgetItem(query.value(city).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 7, item);

        item = new QTableWidgetItem(query.value(postcode).toString());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(table->rowCount() - 1, 8, item);
    }
}

void usersListWnd::searchBtnClicked()
{
    // function that generates sql query for table population
    QString text = searchLine->text();
    QString query;
    if (text.length() > 0){
        if (searchTermCB->currentText() == tr("Postalcode")){
            query = "SELECT * FROM 'Users' WHERE `postal_code` == '" + text +"';";
        } else if (searchTermCB->currentText() == tr("City")){
            query = "SELECT * FROM 'Users' WHERE `city` == '" + text +"';";
        } else if (searchTermCB->currentText() == tr("Number of orders")){
            query = "SELECT * FROM 'Users' WHERE `order_count` > " + text +";";
        } else if (searchTermCB->currentText() == tr("Last ordered above")){
            query = "SELECT * FROM 'Users' WHERE `last_ordered_at` > '" + QDate::fromString(text, "MM-dd-yyyy").toString("yyyy-MM-dd") +"';";
        }
        populateTB(query);
    }
}

void usersListWnd::selectAllClicked(bool checked)
{
    // slot that sets all the checkboxes to "Select all" checkbox state
    for (int i = 0; i < table->rowCount(); i++){
        QCheckBox *cb = qobject_cast<QCheckBox *>(table->cellWidget(i, 0));
        cb->setChecked(checked);
    }
}

void usersListWnd::printBtnClicked()
{
    if (shopSelectCB->currentText().length() == 0 || summLE->text().length() == 0){
        return;
    }
    QPrinter printer;
    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle(tr("Print Document"));

    QPainter painter;

    if (dialog->exec() != QDialog::Accepted) {
        return;
    }
    qDebug() << "print";
    QPixmap *qpix = new QPixmap("images/voucher-bg.png");
    printer.setPageMargins(0, 20, 0, 0, QPrinter::Point);
    painter.begin(&printer);

    for (int i = 0; i < shopsList.length(); i++){
        if (shopsList.at(i)->name == shopSelectCB->currentText()){
            printShop = shopsList.at(i);
        }
    }

    int counter = 0;
    for (int i = 0; i < table->rowCount(); i++){
        QCheckBox *cb = qobject_cast<QCheckBox *>(table->cellWidget(i, 0));
        if (cb->isChecked()){
            int prom = counter % 3;
            if (prom == 0 && counter != 0){
                printer.newPage();
            }

            QString phone = printShop->telephone;
            QString shopName = printShop->name;
            QString address = printShop->street;

            QString name = table->item(i, 3)->text();
            int n = 40;
            painter.drawPixmap(20, prom * qpix->height() + n * prom, 744, 308, *qpix);

            QFont Font = painter.font();
            Font.setPointSizeF(14);
            painter.setFont(Font);
            painter.drawText(128, 195 + prom * qpix->height() + n * prom, 600, 100, Qt::AlignLeft, shopName + "\n" + address);
            painter.drawText(128, 240 + prom * qpix->height() + n * prom, 150, 200, Qt::AlignLeft, phone);

            painter.setPen(QColor(102, 102, 102));
            Font.setPointSizeF(23);
            painter.setFont(Font);
            painter.drawText(70, 62 + prom * qpix->height() + n * prom, 420, 200, Qt::AlignHCenter, name);

            Font.setPointSizeF(48);
            painter.setFont(Font);
            painter.setPen(QColor(40, 40, 40));
            painter.drawText(70, 110 + prom * qpix->height() + n * prom, 420, 200, Qt::AlignHCenter, summLE->text() + " €");

            painter.setPen(QColor(230, 213, 140));
            Font.setPointSizeF(22);
            painter.setFont(Font);

            painter.drawText(10, -65 + prom * qpix->height() + n * prom, 200, 200, Qt::AlignCenter, shopName);

            painter.setPen(QColor(241, 91, 42));
            Font.setPointSizeF(11);
            painter.setFont(Font);

            painter.drawText(340, -68 + prom * qpix->height() + n * prom, 200, 200, Qt::AlignCenter, printShop->code);

            painter.setPen(QColor(Qt::black));
            counter++;
        }
    }

    painter.end();
    this->close();
}
