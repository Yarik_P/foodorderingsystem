//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef USERSLISTWND_H
#define USERSLISTWND_H

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QPushButton>
#include <QtSql>
#include <QGridLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QMessageBox>

#include "product.h"

#include <QtPrintSupport/QPrintDialog>
#include <QPainter>
#include <QtPrintSupport/QPrinter>

#include <QHeaderView>

class usersListWnd : public QWidget
{
    Q_OBJECT
public:
    explicit usersListWnd(QWidget *parent = 0);
    QTableWidget *table;
    QGridLayout *layout;

    QComboBox *searchTermCB;
    QLineEdit *searchLine;
    QPushButton *searchBtn;

    QCheckBox *selectAllCB;
    QComboBox *shopSelectCB;
    QLineEdit *summLE;
    QPushButton *printBtn;

    QSqlDatabase db;

    QList<shop *> shopsList;

    shop *printShop;

    ~usersListWnd();
    void populateTB(QString Query = "");

public slots:
    void searchBtnClicked();
    void selectAllClicked(bool checked);
    void printBtnClicked();
};

#endif // USERSLISTWND_H
