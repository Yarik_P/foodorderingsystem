#include "mainwindow.h"

mainWnd::mainWnd(QWidget *parent)
    : QWidget(parent)
{
    QFile settingsFile("settings.ini");
    bool hasSettings = settingsFile.exists();
    settingsFile.deleteLater();
    QSettings settings("settings.ini", QSettings::IniFormat);

    if (!hasSettings){
        settings.setValue("city", "London");
        settings.setValue("language", "en");
        settings.setValue("paper_type", "monoprint");
        settings.setValue("url", "http://danscom.de/api/shops.json?tokens=ff9e2e49c1afada7c5895d55720c4bfa55bd0094");
        settings.setValue("stylesheet", "styles.css");
        settings.setValue("title", "title");

        settings.beginGroup("Bonuses");
        settings.setValue("10", "Free wine");
        settings.setValue("50", "Free pizza");
    }

    QFile file(settings.value("stylesheet").toStringList().join(","));
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qApp->setStyleSheet(file.readAll());
        file.close();
    }
    file.deleteLater();

    QIcon icon(":/pizza.ico");
    this->setWindowIcon(icon);
    this->setWindowTitle(settings.value("title").toString());
    this->setLocale(QLocale("de_DE"));

    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
        qDebug() << sdb.lastError().text();
    } else {
        QString str = "CREATE TABLE IF NOT EXISTS 'Users' ( `user_uuid` TEXT NOT NULL UNIQUE, `mobile` NUMERIC NOT NULL, `postal_code` TEXT "
                      "DEFAULT NULL, `Name` TEXT, `Street` TEXT, `note` TEXT, `comment` TEXT, `order_count` INTEGER DEFAULT 0, "
                      "`bonus_count` INTEGER DEFAULT 0, `company` TEXT DEFAULT NULL, `department` TEXT DEFAULT NULL, `last_ordered_at` TEXT, "
                      "`created` TEXT, `modified` TEXT, `city` TEXT );";

        QSqlQuery query;
        if (!query.exec(str)){
            qDebug() << query.lastError().text();
        }
        str = "CREATE TABLE IF NOT EXISTS 'orders' ( `order_uid` TEXT UNIQUE, `user_uuid` TEXT, `summ` REAL, `date` TEXT, `order` TEXT, "
              "`order_deleted` INTEGER DEFAULT 0, `driver_id` TEXT, `customer_name` TEXT, `customer_address` TEXT, `order_type` TEXT, `mwst7` "
              "TEXT, `mwst19` TEXT, `shop_id` INTEGER );";
        if (!query.exec(str)){
            qDebug() << query.lastError().text();
        }

        /*str = "CREATE TABLE IF NOT EXISTS 'Expenditures' ( `date` TEXT, `products` TEXT, `products_summ` REAL, `business` TEXT,
         * `business_summ` REAL, `private` TEXT, `private_summ` REAL, `other` TEXT, `other_summ` REAL )";
        if (!query.exec(str)){
            qDebug() << query.lastError().text();
        }*/

        str = "CREATE TABLE IF NOT EXISTS `days` ( `date` TEXT UNIQUE, `cash_begin` REAL, `cash_end` REAL )";
        if (!query.exec(str)){
            qDebug() << query.lastError().text();
        } else {
            /*str = "INSERT INTO `days`(`date`,`cash_begin`) VALUES ('2017-05-01',0.0);";
            if (!query.exec(str)){
                qDebug() << query.lastError().text();
            }*/
        }
        str = "CREATE TABLE IF NOT EXISTS `expenditures2` ( `date` TEXT, `text` TEXT, `summ` REAL, `tax` REAL, `type` INTEGER, `exp_uid` "
              "TEXT UNIQUE )";
        if (!query.exec(str)){
            qDebug() << query.lastError().text();
        }
    }
    sdb.close();


    translator = new QTranslator();
    translator->load("./languages/" + settings.value("language").toString());

    qApp->installTranslator(translator);


    this->resize(610, 230);
    layout = new QGridLayout(this);
    orderBtn = new QPushButton(tr("Order"), this);
    orderBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    reportsBtn = new QPushButton(tr("Reports"), this);
    reportsBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    synchronizeBtn = new QPushButton(tr("Synchronize"), this);
    synchronizeBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    expendituresBtn = new QPushButton(tr("Add expenditure"), this);
    expendituresBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    downloadStreetsBtn = new QPushButton(tr("Download streets"), this);
    downloadStreetsBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    exitBtn = new QPushButton(tr("Exit"), this);
    exitBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    userListBtn = new QPushButton(tr("User list"), this);


    layout->addWidget(orderBtn, 2, 1);
    layout->addWidget(reportsBtn, 2, 2);
    layout->addWidget(synchronizeBtn, 2, 3);
    layout->addWidget(expendituresBtn, 3, 1);
    layout->addWidget(downloadStreetsBtn, 3, 2);
    layout->addWidget(exitBtn, 3, 3);
    layout->addWidget(userListBtn, 4, 1);

    this->setLayout(layout);

    connect(exitBtn, SIGNAL(clicked()), this, SLOT(on_exit_click()));
    connect(orderBtn, SIGNAL(clicked()), this, SLOT(on_make_order_click()));
    connect(reportsBtn, SIGNAL(clicked()), this, SLOT(on_reports_click()));
    connect(expendituresBtn, SIGNAL(clicked()), this, SLOT(on_expenditures_click()));
    connect(synchronizeBtn, SIGNAL(clicked()), this, SLOT(onSyncBtnClicked()));
    connect(downloadStreetsBtn, SIGNAL(clicked()), this, SLOT(onDownloadStreetsClicked()));
    connect(userListBtn, SIGNAL(clicked()), this, SLOT(userListBtnClicked()));

    manager = new QNetworkAccessManager(this);
    if (QFile::exists("shops.json")){
        QFile::remove("shops.json");
    }

    QNetworkRequest request(QUrl(settings.value("url").toStringList().join(",")));
    replyer = manager->get(request);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replying(QNetworkReply*)));
}

mainWnd::~mainWnd()
{
    this->deleteLater();
}

void mainWnd::on_exit_click()
{
    exit(0);
}

void mainWnd::on_make_order_click()
{
    orderList = new ordersWnd();
    orderList->show();
}

void mainWnd::on_reports_click()
{
    repWnd = new reportsWnd();
    repWnd->show();
}

void mainWnd::on_expenditures_click()
{
    expendituresWindow = new expendituresWnd();
    expendituresWindow->show();
}

void mainWnd::onSyncBtnClicked()
{
    backup = new backupWnd();
    backup->show();
}

void mainWnd::onDownloadStreetsClicked()
{
    gs = new getStreetsWnd();
    gs->show();
}

void mainWnd::replying(QNetworkReply* reply)
{
    datas = reply->readAll();
    QFile file("shops.json");

    if (file.open(QIODevice::Append)) {
        QTextStream stream(&file);
        stream << datas;
        file.close();
    }
}

void mainWnd::userListBtnClicked()
{
    userList = new usersListWnd();
    userList->show();
}

