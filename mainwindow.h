//    Food ordering system
//    Copyright (C) 2017

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QSettings>

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QDate>
#include <QSqlDatabase>
#include <QLocale>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>

#include "getStreets.h"
#include "user_input.h"
#include "expenditureswnd.h"
#include "reportswnd.h"
#include "orderswnd.h"
#include "makeorderwnd.h"
#include "backupwnd.h"
#include "userslistwnd.h"

class mainWnd : public QWidget
{
    Q_OBJECT

public:
    QTranslator *translator;
    QString lang;

    mainWnd(QWidget *parent = 0);
    ~mainWnd();
    QGridLayout *layout;
    QPushButton *orderBtn;
    QPushButton *reportsBtn;
    QPushButton *synchronizeBtn;
    QPushButton *expendituresBtn;
    QPushButton *downloadStreetsBtn;
    QPushButton *exitBtn;
    QPushButton *userListBtn;

    user_input *mkOrder;
    reportsWnd *repWnd;
    ordersWnd *orderList;
    expendituresWnd *expendituresWindow;
    backupWnd *backup;
    usersListWnd *userList;


    QNetworkAccessManager *manager;
    QNetworkReply *replyer;
    QByteArray datas;

    QString shopTokens;



    getStreetsWnd *gs;
public slots:
    void on_exit_click();
    void on_make_order_click();
    void on_reports_click();
    void on_expenditures_click();
    void onSyncBtnClicked();
    void onDownloadStreetsClicked();
    void replying(QNetworkReply *reply);
    void userListBtnClicked();
};

#endif // MAINWINDOW_H
