#include "orderswnd.h"

ordersWnd::ordersWnd(QWidget *parent) : QWidget(parent)
{
    this->resize(700, 400);
    this->setWindowTitle("Orders list");
    layout = new QGridLayout(this);
    ordersLB = new QLabel(tr("Orders list"));
    layout->addWidget(ordersLB, 1, 1);

    orderBtn = new QPushButton(tr("Order"));
    layout->addWidget(orderBtn, 2, 1);

    orders = new QTableWidget(0, 5, this);
    layout->addWidget(orders, 3, 1, 1, 3);


    QTableWidgetItem *head_order_id = new QTableWidgetItem(tr("OrderID"));
    QTableWidgetItem *head_name = new QTableWidgetItem(tr("Name"));
    QTableWidgetItem *head_adress = new QTableWidgetItem(tr("Address"));
    QTableWidgetItem *head_driver = new QTableWidgetItem(tr("Driver"));
    QTableWidgetItem *head_action = new QTableWidgetItem(tr("Action"));


    orders->setHorizontalHeaderItem(0, head_order_id);
    orders->setHorizontalHeaderItem(1, head_name);
    orders->setHorizontalHeaderItem(2, head_adress);
    orders->setHorizontalHeaderItem(3, head_driver);
    orders->setHorizontalHeaderItem(4, head_action);

    orders->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    orders->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    orders->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    orders->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    orders->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);

    orders->setSelectionBehavior(QAbstractItemView::SelectRows);

    setTable();
    this->setLayout(layout);

    connect(orderBtn, SIGNAL(clicked()), this, SLOT(orderBtnClicked()));
    QByteArray datas;
    QFile file("shops.json");
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        datas = stream.readAll().toUtf8();
    }

    QJsonDocument doc = QJsonDocument::fromJson(datas);
    QJsonObject object = doc.object();
    QJsonArray shops = object["shops"].toArray();
    foreach (QJsonValue shopJS, shops) {
        QJsonObject obj = shopJS.toObject();
        int id = obj["id"].toInt();
        QString name = obj["name"].toString();
        QString code = obj["code"].toString();
        QString street = obj["street"].toString();
        QString postal_code = obj["postal_code"].toString();
        QString city = obj["city"].toString();
        QString telephone = obj["telephone"].toString();
        QString fax = obj["fax"].toString();
        QString tax_number = obj["tax_number"].toString();
        QJsonArray drivers = obj["drivers"].toArray();

        shop currentShop(id, name, code, street, postal_code, city, telephone, fax, tax_number);
        foreach (QJsonValue driverJS, drivers) {
            QJsonObject driver_obj = driverJS.toObject();
            driver *promDriver = new driver(driver_obj["code"].toString(), driver_obj["name"].toString(), driver_obj["telephone"].toString());
            currentShop.drivers.append(promDriver);
        }
        shopsList.append(currentShop);
    }
}

void ordersWnd::setTable() {

    QSqlDatabase sdb;
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    sdb.open();
    QSqlQuery a_query("select * from orders where `order_deleted`='0';");

    int idName = a_query.record().indexOf("customer_name");
    int idOrderId = a_query.record().indexOf("order_uid");
    int idAddress = a_query.record().indexOf("customer_address");
    int idDriver = a_query.record().indexOf("driver_id");
    int idOrderType = a_query.record().indexOf("order_type");

    while (a_query.next()) {
        QString name = a_query.value(idName).toString();
        QString address = a_query.value(idAddress).toString();
        QString ordertype = a_query.value(idOrderType).toString();

        QTableWidgetItem *nameItem = new QTableWidgetItem(name);
        nameItem->setData(Qt::UserRole, a_query.value(idOrderId).toString());
        QTableWidgetItem *addressItem = new QTableWidgetItem(address);
        QTableWidgetItem *orderIDItem = new QTableWidgetItem(a_query.value(idOrderId).toString());
        nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
        addressItem->setFlags(nameItem->flags());
        orderIDItem->setFlags(nameItem->flags());

        orders->setRowCount(orders->rowCount() + 1);

        orders->setItem(orders->rowCount() - 1, 0, orderIDItem);
        orders->setItem(orders->rowCount() - 1, 1, nameItem);
        orders->setItem(orders->rowCount() - 1, 2, addressItem);


        QString driver = a_query.value(idDriver).toString();
        if (ordertype == "3" || ordertype == "2") {
            QTableWidgetItem *driverItem = new QTableWidgetItem("-");
            driverItem->setFlags(driverItem->flags() ^ Qt::ItemIsEditable);
            orders->setItem(orders->rowCount() - 1, 3, driverItem);
        } else {
            if (driver.length() == 0) {
                if (ordertype == "3" || ordertype == "2") {
                } else {
                    QPushButton *setDriverBtn = new QPushButton("+");
                    orders->setCellWidget(orders->rowCount() - 1, 3, setDriverBtn);
                    connect(setDriverBtn, SIGNAL(clicked()), this, SLOT(addDriverBtnClicked()));
                }
            } else {
                QTableWidgetItem *driverItem = new QTableWidgetItem(driver);
                driverItem->setFlags(driverItem->flags() ^ Qt::ItemIsEditable);
                orders->setItem(orders->rowCount() - 1, 3, driverItem);
            }
        }
        QComboBox *cb = new QComboBox(this);
        cb->addItem(tr("Chose action"));
        cb->addItem(tr("Print"));
        cb->addItem(tr("Cancel"));
        orders->setCellWidget(orders->rowCount() - 1, 4, cb);
        connect(cb, SIGNAL(currentTextChanged(QString)), this, SLOT(actionChanged(QString)));
    }
}

void ordersWnd::orderBtnClicked()
{
    q = new user_input();
    connect(q, SIGNAL(destroyed(QObject*)), this, SLOT(close()));
    q->show();
    this->close();
}

void ordersWnd::addDriverBtnClicked()
{
    for (int row = 0; row < orders->rowCount(); row++) {
        if (sender() == orders->cellWidget(row, 3)) {
            QSqlDatabase sdb1;
            sdb1 = QSqlDatabase::addDatabase("QSQLITE");
            sdb1.setDatabaseName("azzip.db");
            sdb1.open();
            QSqlQuery b("select shop_id from orders where order_uid like '" + orders->item(row, 1)->data(Qt::UserRole).toString() + "';");
            b.next();
            setDriverWnd *dr = new setDriverWnd();
            int shopid = b.record().value(0).toInt();
            for (int i = 0; i<5;i++) {
                qDebug() << b.record().value(i).toString();
            }

            for (int i = 0; i < shopsList.length(); i++) {
                if (shopid == shopsList.at(i).id) {
                    for (int j = 0; j < shopsList.at(i).drivers.length(); j++) {
                        dr->driversCB->addItem(shopsList.at(i).drivers.at(j)->code);
                    }
                }
            }

            dr->exec();
            if (dr->driversCB->currentText().length() != 0) {
                QTableWidgetItem *driverItem = new QTableWidgetItem(dr->driversCB->currentText());
                driverItem->setFlags(driverItem->flags() ^ Qt::ItemIsEditable);
                QSqlQuery a("UPDATE `orders` SET `driver_id`='" + dr->driversCB->currentText() + "' WHERE order_uid like '" +
                            orders->item(row, 1)->data(Qt::UserRole).toString() + "';");

                if(!a.exec()) {
                    qDebug() << "Berror:  " << a.lastError();
                }
                orders->removeCellWidget(row, 3);
                orders->setItem(row, 3, driverItem);
            } else {
                QMessageBox::information(this, tr("Error!"), tr("No driver to set"));
            }
            sdb1.close();
        }
    }
}

void ordersWnd::actionChanged(QString action) {
    for (int row = 0; row < orders->rowCount(); row++) {
        if (sender() == orders->cellWidget(row, 4)) {
            if (action == tr("Cancel")) {
                int q = QMessageBox::question(this, tr("Delete confirmation"), tr("Are you shure want to delete?"),
                                              QMessageBox::Yes|QMessageBox::No);
                if (q == QMessageBox::Yes) {
                    QSqlDatabase sdb1;
                    sdb1 = QSqlDatabase::addDatabase("QSQLITE");
                    sdb1.setDatabaseName("azzip.db");
                    sdb1.open();
                    qDebug() << sdb1.defaultConnection;
                    QSqlQuery a("UPDATE `orders` SET `order_deleted`='1' WHERE order_uid like '" +
                                orders->item(row, 1)->data(Qt::UserRole).toString() + "';");
                    qDebug() << "UPDATE `orders` SET `order_deleted`='1' WHERE order_uid='" +
                                orders->item(row, 1)->data(Qt::UserRole).toString() + "';";
                    if(!a.exec()) {
                        qDebug() << "Berror:  " << a.lastError();
                    }
                    orders->removeRow(row);
                    sdb1.close();
                }
            }
            if (action == "edit") {
                int q = QMessageBox::question(this, tr("Edition confirmation"), tr("Are you shure want to edit?"),
                                              QMessageBox::Yes|QMessageBox::No);
                if (q == QMessageBox::Yes) {
                    QSqlDatabase sdb1;
                    sdb1 = QSqlDatabase::addDatabase("QSQLITE");
                    sdb1.setDatabaseName("azzip.db");
                    sdb1.open();

                    QSqlQuery a("select * from `orders` WHERE order_uid like '" + orders->item(row, 1)->data(Qt::UserRole).toString() + "';");
                    a.next();
                    int idOrderId = a.record().indexOf("order_uid");
                    int idUserId = a.record().indexOf("user_uuid");
                    int idSumm = a.record().indexOf("summ");
                    int idOrder = a.record().indexOf("order");
                    int idCustName = a.record().indexOf("customer_name");
                    int idCustAddress = a.record().indexOf("customer_address");
                    int idOrderType = a.record().indexOf("order_type");
                    QString order_type;
                    idOrderType = a.value(idOrderType).toInt();
                    if (idOrderType == 1) {
                        order_type = tr("Delivery");
                    } else if (idOrderType == 2) {
                        order_type = tr("Take away");
                    } else {
                        order_type = tr("In Restaurant");
                    }

                    makeOrderWnd *q = new makeOrderWnd(a.value(idOrder).toString(), order_type);
                    q->userId = a.value(idUserId).toString();
                    q->username = a.value(idCustName).toString();
                    q->address = a.value(idCustAddress).toString();
                    q->order_uid = a.value(idOrderId).toString();
                    q->finalSum = a.value(idSumm).toFloat();
                    q->orderTypeStr = order_type;

                    QSqlQuery b("select * from `users` WHERE user_uuid like '" + q->userId + "';");
                    b.next();

                    int idName = b.record().indexOf("Name");
                    int idStreet = b.record().indexOf("Street");
                    int idpostal_code = b.record().indexOf("postal_code");
                    int idmobile = b.record().indexOf("mobile");
                    int idcity = b.record().indexOf("city");
                    q->city = b.value(idcity).toString();
                    q->username = b.value(idName).toString();
                    q->user_phone = b.value(idmobile).toString();
                    q->post_code = b.value(idpostal_code).toString();
                    q->address = b.value(idStreet).toString();

                    q->show();

                    sdb1.close();
                    this->close();
                }
            }
            if (action == tr("Print")) {
                int q = QMessageBox::question(this, tr("Print confirmation"), tr("Are you shure want to print?"),
                                              QMessageBox::Yes|QMessageBox::No);
                if (q == QMessageBox::Yes) {
                    QSqlDatabase sdb1;
                    sdb1 = QSqlDatabase::addDatabase("QSQLITE");
                    sdb1.setDatabaseName("azzip.db");
                    sdb1.open();

                    QSqlQuery a("select * from `orders` WHERE order_uid like '" + orders->item(row, 1)->data(Qt::UserRole).toString() + "';");
                    a.next();


                    int idUserId = a.record().indexOf("user_uuid");
                    int idmwst7 = a.record().indexOf("mwst7");
                    int idmwst19 = a.record().indexOf("mwst19");
                    int idOrder = a.record().indexOf("order");
                    int idSumm = a.record().indexOf("summ");
                    int idShopId = a.record().indexOf("shop_id");
                    int idOrderType = a.record().indexOf("order_type");


                    int shopid = a.record().value(idShopId).toInt();


                    QMap<QString, int> ordList;
                    QStringList splitStr = a.value(idOrder).toString().split(";");
                    for (int i = 0; i < splitStr.length() - 1; i++) {
                        QStringList prom = splitStr.at(i).split(", ");
                        ordList.insert(prom.at(0) + " / " + prom.at(4), prom.at(1).toInt());
                    }

                    QSqlQuery b("select * from `users` WHERE user_uuid like '" + a.value(idUserId).toString() + "';");
                    b.next();

                    int idName = b.record().indexOf("Name");
                    int idStreet = b.record().indexOf("Street");
                    int idpostal_code = b.record().indexOf("postal_code");
                    int idmobile = b.record().indexOf("mobile");
                    int idcity = b.record().indexOf("city");

                    int orderCount = b.value(b.record().indexOf("order_count")).toInt();


                    user Usr = user(b.value(idName).toString(), b.value(idStreet).toString(), b.value(idpostal_code).toString(),
                                    b.value(idcity).toString(), b.value(idmobile).toString());

                    for (int i = 0; i < shopsList.length(); i++) {
                        if (shopid == shopsList.at(i).id) {
                            QSettings settings("settings.ini", QSettings::IniFormat);
                            if (settings.value("paper_type").toString() == "monoprint") {
                                makeOrderWnd::print_order2(ordList, shopsList.at(i), a.value(idSumm).toString(), orderCount);
                            } else {
                                bool showInfo = true ? a.record().value(idOrderType).toInt() == 1 : false;
                                makeOrderWnd::print_order(ordList, shopsList.at(i), Usr, a.value(idmwst7).toString(),
                                                          a.value(idmwst19).toString(), a.value(idSumm).toString(), showInfo);
                            }
                        }
                    }
                    sdb1.close();
                    this->close();
                }
            }
        }
    }
}
