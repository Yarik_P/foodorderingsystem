#include "getStreets.h"

getStreetsWnd::getStreetsWnd(QWidget *parent)
    : QWidget(parent)
{
    layout = new QGridLayout(this);
    strNameLE = new QLineEdit();
    okBtn = new QPushButton("Ok", this);
    workLabel = new QLabel(this);
    layout->addWidget(strNameLE, 2, 1);
    layout->addWidget(okBtn, 3, 1);
    layout->addWidget(workLabel, 4, 1);

    this->setLayout(layout);
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
    connect(okBtn, SIGNAL(clicked()), this, SLOT(okBtnClicked()));
}

getStreetsWnd::~getStreetsWnd()
{

}

void getStreetsWnd::replyFinished(QNetworkReply *reply)
{
    QByteArray replytext = reply->readAll();
    QString filename = cityName + ".txt";

    QRegularExpression reA("<tag k=\"name\" v=\"([^<]+)\"\/>");

    QStringList q;
    QRegularExpressionMatchIterator i = reA.globalMatch(replytext);
    while (i.hasNext()) {
        QRegularExpressionMatch match = i.next();
        if (match.hasMatch()) {
            QString matchq = match.captured(1);
            if (!q.contains(matchq)){
                q.append(matchq);
            }
        }
    }


    QFile file(filename);
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        foreach (QString street, q) {
            stream << street << endl;
        }
    }
    this->close();
}

void getStreetsWnd::okBtnClicked()
{
    cityName = strNameLE->text();
    manager->post(QNetworkRequest(QUrl("http://www.overpass-api.de/api/interpreter")), "area[name~'" +
                  cityName.toUtf8() + "'];\nway(area)[highway][name];\nout;");
    okBtn->setEnabled(false);
    workLabel->setText("Working in progress....");
}
