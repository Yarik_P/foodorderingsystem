#include "user_input.h"

user_input::user_input() : QWidget(0)
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    this->setWindowTitle(tr("Choosing customer"));
    userID = "";
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
        qDebug() << sdb.lastError().text();
    }
    QSqlQuery query("select mobile, name from users where 1");

    int idPhone = query.record().indexOf("mobile");
    int idName = query.record().indexOf("name");
    while (query.next()) {
        QString phone = query.value(idPhone).toString();
        QString name = query.value(idName).toString();
        phones.append(phone);
        names.append(name);
    }
    sdb.close();

    QStringList nameFilter("*.txt");
    QDir directory(".");
    QStringList txtFilesAndDirectories = directory.entryList(nameFilter);
    QStringList cities;
    foreach (QString filename, txtFilesAndDirectories) {
        cities.append(filename.split(".").at(0));
    }

    QCompleter* completer = new QCompleter(phones);

    QCompleter* completerCities = new QCompleter(cities);

    QCompleter *completerNames = new QCompleter(names);


    this->resize(700, 350);
    layout = new QGridLayout(this);
    mkOrder = new QPushButton(tr("Order"));
    layout->addWidget(mkOrder, 8, 6, 1, 1);
    telefonLB = new QLabel(tr("Telephone"));
    layout->addWidget(telefonLB, 2, 1);
    kundeNrLB = new QLabel(tr("User ID"));
    kundeNrLB->setAlignment(Qt::AlignCenter|Qt::AlignRight);
    layout->addWidget(kundeNrLB, 2, 5);
    nameLB = new QLabel(tr("Name"));
    layout->addWidget(nameLB, 3, 1);
    strasseLB = new QLabel(tr("Street"));
    layout->addWidget(strasseLB, 4, 1);
    bemerkungLB = new QLabel(tr("comment"));
    layout->addWidget(bemerkungLB, 6, 1);
    hinweisLB = new QLabel(tr("note"));
    layout->addWidget(hinweisLB, 7, 1);
    cityLB = new QLabel(tr("City"));
    layout->addWidget(cityLB, 5, 5);
    QLabel *postalCodeLB = new QLabel(tr("Postal code"));
    layout->addWidget(postalCodeLB, 5, 1);


    telefonLE = new QLineEdit();
    telefonLE->setCompleter(completer);
    layout->addWidget(telefonLE, 2, 2, 1, 3);
    userIdLE = new QLineEdit();
    layout->addWidget(userIdLE, 2, 6);
    nameLE = new QLineEdit();
    nameLE->setCompleter(completerNames);
    layout->addWidget(nameLE, 3, 2, 1, 5);
    strasseLE1 = new QLineEdit();
    layout->addWidget(strasseLE1, 4, 2, 1, 5);


    postalCodeLE = new QLineEdit();
    layout->addWidget(postalCodeLE, 5, 2, 1, 2);
    cityLE = new QLineEdit();
    cityLE->setCompleter(completerCities);
    layout->addWidget(cityLE, 5, 6, 1, 1);
    commentLE = new QLineEdit();
    layout->addWidget(commentLE, 6, 2, 1, 5);
    noteLE = new QLineEdit();
    layout->addWidget(noteLE, 7, 2, 1, 5);


    orderType = new QComboBox(this);
    orderType->addItem(tr("Delivery"));
    orderType->addItem(tr("Take away"));
    orderType->addItem(tr("In Restaurant"));

    layout->addWidget(orderType, 8, 2, 1, 2);


    mkOrder->setDefault(true);
    this->setLayout(layout);
    connect(mkOrder, SIGNAL(clicked()), this, SLOT(orderBtnClicked()));
    connect(telefonLE, SIGNAL(editingFinished()), this, SLOT(telefonChanged()));
    connect(nameLE, SIGNAL(editingFinished()), this, SLOT(nameChanged()));

    connect(cityLE, SIGNAL(editingFinished()), this, SLOT(cityChanged()));
    connect(strasseLE1, SIGNAL(editingFinished()), this, SLOT(streetChanged()));
    cityLE->setText(settings.value("city").toString());
    cityChanged();
    connect(orderType, SIGNAL(currentTextChanged(QString)), this, SLOT(orderTypeChanged(QString)));

}

void user_input::orderBtnClicked() {
    userID = userIdLE->text();
    if (userID.length() > 0) {
        makeOrderWnd *q = new makeOrderWnd("", orderType->currentText());
        qDebug() << userID << nameLE->text() << strasseLE1->text() << postalCodeLE->text() << cityLE->text() << telefonLE->text();
        q->userId = userID;
        q->username = nameLE->text();
        q->address = strasseLE1->text();
        q->post_code = postalCodeLE->text();
        q->city = cityLE->text();
        q->user_phone = telefonLE->text();
        q->show();
        this->close();
    } else {
        QString toCrypt;
        if (orderType->currentText() == tr("Delivery")) {
            if (strasseLE1->text().length() > 5) {
                toCrypt = strasseLE1->text() + QDateTime::currentDateTime().toString();
            } else {
                QMessageBox::information(this, tr("Error"), tr("address must be longer"));
                return;
            }
        } else {
            toCrypt = nameLE->text() + QDateTime::currentDateTime().toString();
        }

        if (!sdb.open()) {
            qDebug() << sdb.lastError().text();
        }
        QSqlQuery a_query;

        a_query.prepare("INSERT INTO `Users`(`user_uuid`,`mobile`,`postal_code`,`Name`,`Street`,`note`,`comment`,`last_ordered_at`,`created`,`modified`,`city`) "
                        "VALUES (:user_uuid,:mobile,:postal_code,:name,:street,:note,:comment,0,:date_created,NULL,:city);");
        userIdLE->setText(QCryptographicHash::hash(toCrypt.toUtf8(), QCryptographicHash::Md5).toHex());
        a_query.bindValue(":user_uuid", userIdLE->text());
        a_query.bindValue(":mobile", telefonLE->text());
        a_query.bindValue(":name", nameLE->text());
        a_query.bindValue(":street", strasseLE1->text());
        a_query.bindValue(":note", noteLE->text());
        a_query.bindValue(":comment", commentLE->text());
        a_query.bindValue(":date_created", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
        a_query.bindValue(":postal_code", postalCodeLE->text());
        a_query.bindValue(":city", cityLE->text());

        if(!a_query.exec())
        {
            qDebug() << "error:  " << a_query.lastError();
        }

        sdb.close();
        orderBtnClicked();
    }
}

void user_input::telefonChanged()
{
    if (!(telefonLE->text() == lastPhone)) {
        if (!sdb.open()) {
            qDebug() << sdb.lastError().text();
        }
        QSqlQuery query(QString("select * from users where mobile = '" + telefonLE->text() + "';"));

        if (query.next()) {
            int idUid = query.record().indexOf("user_uuid");
            int idName = query.record().indexOf("Name");
            int idStreet = query.record().indexOf("Street");
            int idComment = query.record().indexOf("comment");
            int idNote = query.record().indexOf("note");
            int idCity = query.record().indexOf("city");
            int idPostCode = query.record().indexOf("postal_code");

            QString value = query.value(idName).toString();
            if (value.length() > 0) {
                nameLE->setText(value);
            } else {
                nameLE->clear();
            }
            value = query.value(idCity).toString();
            if (value.length() > 0) {
                cityLE->setText(value);
            } else {
                cityLE->clear();
            }
            value = query.value(idPostCode).toString();
            if (value.length() > 0) {
                postalCodeLE->setText(value);
            } else {
                postalCodeLE->clear();
            }
            value = query.value(idStreet).toString();
            if (value.length() > 0) {
                strasseLE1->setText(value);
            } else {
                strasseLE1->clear();
            }
            value = query.value(idComment).toString();
            if (value.length() > 0) {
                commentLE->setText(value);
            } else {
                commentLE->clear();
            }
            value = query.value(idNote).toString();
            if (value.length() > 0) {
                noteLE->setText(value);
            } else {
                noteLE->clear();
            }

            userID = query.value(idUid).toString();
            userIdLE->setText(userID);
        } else {
            userIdLE->clear();
            strasseLE1->clear();
            noteLE->clear();
            commentLE->clear();
            userID = "";
        }
        sdb.close();
        lastPhone = telefonLE->text();
    }
}

void user_input::nameChanged()
{
    if (!(nameLE->text() == lastName)) {
        if (!sdb.open()) {
            qDebug() << sdb.lastError().text();
        }
        QSqlQuery query(QString("select * from users where name = '" + nameLE->text() + "';"));

        if (query.next()) {
            int idUid = query.record().indexOf("user_uuid");
            int idStreet = query.record().indexOf("Street");
            int idComment = query.record().indexOf("comment");
            int idNote = query.record().indexOf("note");
            int idCity = query.record().indexOf("city");
            int idPostCode = query.record().indexOf("postal_code");
            int idPhone = query.record().indexOf("mobile");

            QString value = query.value(idPhone).toString();
            if (value.length() > 0) {
                telefonLE->setText(value);
            } else {
                telefonLE->clear();
            }
            value = query.value(idCity).toString();
            if (value.length() > 0) {
                cityLE->setText(value);
            } else {
                cityLE->clear();
            }
            value = query.value(idPostCode).toString();
            if (value.length() > 0) {
                postalCodeLE->setText(value);
            } else {
                postalCodeLE->clear();
            }
            value = query.value(idStreet).toString();
            if (value.length() > 0) {
                strasseLE1->setText(value);
            } else {
                strasseLE1->clear();
            }
            value = query.value(idComment).toString();
            if (value.length() > 0) {
                commentLE->setText(value);
            } else {
                commentLE->clear();
            }
            value = query.value(idNote).toString();
            if (value.length() > 0) {
                noteLE->setText(value);
            } else {
                noteLE->clear();
            }
            userID = query.value(idUid).toString();
            userIdLE->setText(userID);
        } else {
            userIdLE->clear();
            strasseLE1->clear();
            noteLE->clear();
            commentLE->clear();
            userID = "";
        }
        sdb.close();
        lastName = nameLE->text();
    }
}

void user_input::keyPressEvent(QKeyEvent *event)
{
    if (userIdLE->text().length() > 0) {
        switch (event->key())
        {
        case Qt::Key_Enter:
            mkOrder->setFocus();
            break;
        case Qt::Key_Return:
            mkOrder->setFocus();
            break;
        }
    }
}

void user_input::orderTypeChanged(QString text)
{
    if (text == tr("Take away") || text == tr("In Restaurant")) {
        setTabOrder(nameLE, mkOrder);
    } else {
        setTabOrder(noteLE, mkOrder);
    }
}

void user_input::cityChanged()
{
    streets.clear();
    QFile file(cityLE->text() + ".txt");
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        while (true)
        {
            QString line = stream.readLine();
            if (line.isNull())
                break;
            else
                streets.append(line);
        }
        file.close();
    }
    QCompleter *streetsCompleter = new QCompleter(streets);
    streetsCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    strasseLE1->setCompleter(streetsCompleter);
}

void user_input::streetChanged()
{
    QString text = strasseLE1->text();
    if (!streets.contains(text)) {
        QFile file(cityLE->text() + ".txt");
        if (file.open(QIODevice::Append)) {
            file.write(text.toUtf8() + "\n");
        }
        file.close();
    }
}
