#include "reportswnd.h"

reportsWnd::reportsWnd(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle("Reports");
    layout = new QGridLayout(this);
    this->resize(600, 300);
    dateBegin = new QLabel(tr("Date start"), this);
    dateBegin->setAlignment(Qt::AlignRight|Qt::AlignCenter);
    dateBeginDE = new QDateEdit(QDate::currentDate(), this);
    dateBeginDE->setCalendarPopup(true);

    layout->addWidget(dateBegin, 2, 1);
    layout->addWidget(dateBeginDE, 2, 2);

    dateEnd = new QLabel(tr("Date end"), this);
    dateEnd->setAlignment(Qt::AlignRight|Qt::AlignCenter);

    dateEndDE = new QDateEdit(QDate::currentDate(), this);
    dateEndDE->setCalendarPopup(true);

    layout->addWidget(dateEnd, 2, 3);
    layout->addWidget(dateEndDE, 2, 4);

    dailyReportBtn = new QPushButton(tr("Tax report"));

    driverCB = new QComboBox(this);
    driverCB->addItem(tr("All"));

    layout->addWidget(dailyReportBtn, 1, 4);
    layout->addWidget(driverCB, 2, 5);

    statTable = new QTableWidget(0, 5);
    statTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    layout->addWidget(statTable, 3, 1, 4, 6);

    QTableWidgetItem *head_blank = new QTableWidgetItem();
    QTableWidgetItem *head_date = new QTableWidgetItem(tr("Date"));
    QTableWidgetItem *head_count = new QTableWidgetItem(tr("Order ID"));
    QTableWidgetItem *head_sum = new QTableWidgetItem(tr("Sum"));
    QTableWidgetItem *head_mwst = new QTableWidgetItem(tr("MwSt"));

    statTable->setHorizontalHeaderItem(0, head_blank);


    statTable->setHorizontalHeaderItem(1, head_count);
    statTable->setHorizontalHeaderItem(2, head_date);
    statTable->setHorizontalHeaderItem(3, head_mwst);
    statTable->setHorizontalHeaderItem(4, head_sum);
    for (int i = 1; i < statTable->horizontalHeader()->count(); i++) {
        statTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
    }

    monthStat = new QPushButton(tr("Month report"), this);
    printBtn = new QPushButton(tr("Print"), this);
    someBtn = new QPushButton(tr("Search"), this);

    layout->addWidget(monthStat, 1, 5);
    layout->addWidget(printBtn, 1, 6);
    layout->addWidget(someBtn, 2, 6);

    this->setLayout(layout);
    populateTable("SELECT * FROM orders");
    connect(someBtn, SIGNAL(clicked()), this, SLOT(searchButtonClicked()));
    connect(monthStat, SIGNAL(clicked()), this, SLOT(monthStatClicked()));

    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
        qDebug() << sdb.lastError().text();
    }

    QSqlQuery query("select distinct `driver_id` from orders where 1;");
    while (query.next()) {
        if (query.value(0).toString().length() != 0) {
            driverCB->addItem(query.value(0).toString());
        }
    }
    sdb.close();

    connect(driverCB, SIGNAL(currentTextChanged(QString)), this, SLOT(driverChanged(QString)));
    connect(dailyReportBtn, SIGNAL(clicked()), this, SLOT(dailyReportBtnClicked()));
    connect(printBtn, SIGNAL(clicked()), this, SLOT(printBtnClicked()));
}

void reportsWnd::populateTable(QString _query) {
    QLocale german(QLocale::German, QLocale::Germany);
    statTable->setRowCount(0);

    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("azzip.db");
    if (!sdb.open()) {
        qDebug() << sdb.lastError().text();
    }
    query = QSqlQuery(_query);

    int idName = query.record().indexOf("order_uid");
    int idQuantity = query.record().indexOf("summ");
    int idDate = query.record().indexOf("date");
    int idmwst7 = query.record().indexOf("mwst7");
    int idmwst19 = query.record().indexOf("mwst19");
    while (query.next()) {
        QString name = query.value(idName).toString();
        QString quantity = german.toString(query.value(idQuantity).toFloat(), 'f', 2);
        QString date = QDate::fromString(query.value(idDate).toString(), "yyyy-MM-dd").toString("dd.MM.yyyy");
        statTable->setRowCount(statTable->rowCount()+1);

        QString MWST = german.toString(query.value(idmwst7).toFloat() + query.value(idmwst19).toFloat(), 'f', 2);


        QTableWidgetItem *q = new QTableWidgetItem(date);
        QTableWidgetItem *w = new QTableWidgetItem(name);
        QTableWidgetItem *mwst = new QTableWidgetItem(MWST);
        QTableWidgetItem *e = new QTableWidgetItem(quantity);


        statTable->setItem(statTable->rowCount() - 1, 2, q);
        statTable->setItem(statTable->rowCount() - 1, 1, w);
        statTable->setItem(statTable->rowCount() - 1, 3, mwst);
        statTable->setItem(statTable->rowCount() - 1, 4, e);
    }
    if (statTable->rowCount() == 0) {
        statTable->horizontalHeader()->resizeSection(0, 14);
    } else {
        statTable->horizontalHeader()->resizeSection(0, 0);
    }
}

void reportsWnd::searchButtonClicked() {
    QString date1 = dateBeginDE->date().toString("yyyy-MM-dd");
    QString date2 = dateEndDE->date().toString("yyyy-MM-dd");
    populateTable("SELECT * FROM orders WHERE `date` BETWEEN '" + date1 + "' AND '" + date2 + "';");
}


void reportsWnd::monthStatClicked() {
    q = new month_stat();
    q->show();
}

void reportsWnd::driverChanged(QString str) {
    QString date1 = dateBeginDE->date().toString("yyyy-MM-dd");
    QString date2 = dateEndDE->date().toString("yyyy-MM-dd");
    if (str == tr("All")) {
        populateTable("SELECT * FROM orders WHERE `date` BETWEEN '" + date1 + "' AND '" + date2 + "';");
    } else {
        populateTable("SELECT * FROM orders WHERE `date` BETWEEN '" + date1 + "' AND '" + date2 + "' AND `driver_id`='" + str + "';");
    }
}

void reportsWnd::dailyReportBtnClicked() {
    z = new dailyStatWnd();
    z->show();
}

void reportsWnd::printBtnClicked() {
    // prints all selected orders to excel file
    // int("A") = 65
    QXlsx::Document xlsx;

    query.first();
    int counter = 1;
    xlsx.write("A" + QString::number(counter), "order_uid");
    xlsx.write("B" + QString::number(counter), "user_uuid");
    xlsx.write("C" + QString::number(counter), "summ");
    xlsx.write("D" + QString::number(counter), "date");
    xlsx.write("E" + QString::number(counter), "order");
    xlsx.write("F" + QString::number(counter), "order_deleted");
    xlsx.write("G" + QString::number(counter), "driver_id");
    xlsx.write("H" + QString::number(counter), "customer_name");
    xlsx.write("I" + QString::number(counter), "Address");
    xlsx.write("J" + QString::number(counter), "order type");
    xlsx.write("K" + QString::number(counter), "mwst7");
    xlsx.write("L" + QString::number(counter), "mwst19");
    xlsx.write("M" + QString::number(counter), "shop id");

    counter++;

    int i;
    int j;
    for (i = 65, j = 0; j < 13; i++, j++) {
        xlsx.write(QChar(i) + QString::number(counter), query.value(j));
    }
    counter++;
    while(query.next()) {
        for (i = 65, j = 0; j < 13; i++, j++) {
            xlsx.write(QChar(i) + QString::number(counter), query.value(j));
        }
        counter++;
    }
    xlsx.saveAs("orders.xlsx");
    QMessageBox::information(this, tr("Success"), tr("Orders where saved to excel file."));
}
